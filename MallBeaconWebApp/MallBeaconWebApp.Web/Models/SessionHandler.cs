﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MallBeaconWebApp.Web.Models
{
    public class SessionHandler : ActionFilterAttribute
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["MallManagmentDb"].ConnectionString;
        public bool IsAuthRequire = false;
        public static string HashedSalt = "mkdV47yoOnt/24YtW5dd6b3r3Fzu+2X6PlJ1Q4BgB3M=";
        public static string ServiceEmail = "";
        public static string ServiceEmailPassword = "";
        public static int TokenExpirationTime = 30;//Consider as minute

        public SessionHandler(bool checkSession)
        {
            IsAuthRequire = checkSession;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Check authentication code
            if (IsAuthRequire)
            {
                var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                if (HttpContext.Current.Session["Merchant"] != null)
                {
                    var isValidToken = ValidateToken(HttpContext.Current.Session["Merchant"].ToString());
                    if (!isValidToken)
                    {
                        ClearSession();
                        filterContext.Result = new RedirectResult("~/Home/AdminLogin");
                        return;
                    }
                    if (controllerName != "Merchant" && controllerName != "Home")
                    {
                        filterContext.Result = new RedirectResult("~/Home/AccessRestrict");
                        return;
                    }
                }
                else if (HttpContext.Current.Session["Admin"] != null)
                {
                    var isValidToken = ValidateToken(HttpContext.Current.Session["Admin"].ToString());
                    if (!isValidToken)
                    {
                        ClearSession();
                        filterContext.Result = new RedirectResult("~/Home/AdminLogin");
                        return;
                    }
                    if (controllerName != "Admin" && controllerName != "AdminMaster" && controllerName != "Home")
                    {
                        filterContext.Result = new RedirectResult("~/Home/AccessRestrict");
                        return;
                    }
                }
                else if (HttpContext.Current.Session["FranchiesUser"] != null)
                {
                    var isValidToken = ValidateToken(HttpContext.Current.Session["FranchiesUser"].ToString());
                    if (!isValidToken)
                    {
                        ClearSession();
                        filterContext.Result = new RedirectResult("~/Home/AdminLogin");
                        return;
                    }
                    if (controllerName != "Franchies" && controllerName != "Home")
                    {
                        filterContext.Result = new RedirectResult("~/Home/AccessRestrict");
                        return;
                    }
                }
                else
                {
                    ClearSession();
                    filterContext.Result = new RedirectResult("~/Home/AdminLogin");
                    return;
                }
            }
            base.OnActionExecuting(filterContext);
            Log("OnActionExecuting", filterContext.RouteData);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var errorMsg = filterContext.Controller.TempData != null && filterContext.Controller.TempData["Error"] != null
                            ? filterContext.Controller.TempData["Error"].ToString() : null;
            filterContext.Controller.TempData.Remove("ErrorMessage");
            if (!string.IsNullOrEmpty(errorMsg))
            {
                filterContext.Controller.TempData.Add("ErrorMessage", errorMsg);
            }
            else
            {
                filterContext.Controller.TempData.Add("ErrorMessage", null);
            }

            var successMsg = filterContext.Controller.TempData != null && filterContext.Controller.TempData["Success"] != null
                ? filterContext.Controller.TempData["Success"].ToString() : null;
            filterContext.Controller.TempData.Remove("SuccessMessage");
            if (!string.IsNullOrEmpty(successMsg))
                filterContext.Controller.TempData.Add("SuccessMessage", successMsg);
            else
                filterContext.Controller.TempData.Add("SuccessMessage", null);

            Log("OnActionExecuted", filterContext.RouteData);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Log("OnResultExecuted", filterContext.RouteData);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Log("OnResultExecuting ", filterContext.RouteData);
        }

        private void Log(string methodName, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
            Debug.WriteLine(message, "Action Filter Log");
        }

        #region ClearSession
        public static void ClearSession()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
        }
        #endregion

        #region HashedPasswordEncryption
        public static byte[] Combine(byte[] first, byte[] second)
        {
            var ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
        public static byte[] HashPasswordWithSalt(byte[] toBeHashed, byte[] salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var combinedHash = Combine(toBeHashed, salt);
                return sha256.ComputeHash(combinedHash);
            }
        }
        #endregion

        #region GenerateAlphabetic
        private static Random random = new Random();
        public static string RandomString()
        {
            var length = 6;
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion

        #region GeneralMailer
        public static void GeneralMailer(string toEmail, string subject, string body, bool isBodyHtml)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                mail.From = new MailAddress(ServiceEmail);
                mail.To.Add(toEmail);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = isBodyHtml;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(ServiceEmail, ServiceEmailPassword);
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            { }
        }

        public static string PopulateBody(string bodyText, string LinkText, string URL, string SpecialText)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.Web.HttpContext.Current.Server.MapPath("~/Views/GeneralEmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("@Body", bodyText);
            if (!string.IsNullOrEmpty(URL) && !string.IsNullOrEmpty(LinkText))
            {
                body = body.Replace("@LinkDisplayOption", "block");
                body = body.Replace("@LinkURL", URL);
                body = body.Replace("@LinkText", LinkText);
            }
            else
            {
                body = body.Replace("@LinkDisplayOption", "none");
            }
            if (!string.IsNullOrEmpty(SpecialText))
            {
                body = body.Replace("@SpecialDisplayOption", "block");
                body = body.Replace("@SpecialText", SpecialText);
            }
            else
            {
                body = body.Replace("@SpecialDisplayOption", "none");
            }
            return body;
        }
        #endregion

        #region TokenActions
        public static string GenerateToken(string username, string userid)
        {
            string token = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(username))
                {
                    var ticks = DateTime.Now.Ticks;
                    string actualData = string.Join(":", new string[] { username, ticks.ToString(), userid });
                    byte[] actualByteData = Encoding.UTF8.GetBytes(actualData);
                    token = Convert.ToBase64String(actualByteData);
                }
                else if (!string.IsNullOrEmpty(username))
                {
                    var ticks = DateTime.Now.Ticks;
                    string actualData = string.Join(":", new string[] { username, ticks.ToString() });
                    byte[] actualByteData = Encoding.UTF8.GetBytes(actualData);
                    token = Convert.ToBase64String(actualByteData);
                }
            }
            catch (Exception ex)
            {
                token = string.Empty;
            }
            return token;
        }

        public static bool ValidateToken(string token)
        {
            bool result = false;
            try
            {
                byte[] tokenByte = Convert.FromBase64String(token);
                string actualString = Encoding.UTF8.GetString(tokenByte);
                if (!string.IsNullOrEmpty(actualString))
                {
                    var timestamp = new DateTime(long.Parse(actualString.Split(':')[1]));
                    var currentTime = DateTime.Now;
                    TimeSpan ts = currentTime - timestamp;
                    result = ts.TotalMinutes < TokenExpirationTime;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static string GetUsernameFromToken(string token)
        {
            var result = string.Empty;
            try
            {
                byte[] tokenByte = Convert.FromBase64String(token);
                string actualString = Encoding.UTF8.GetString(tokenByte);
                if (!string.IsNullOrEmpty(actualString))
                {
                    result = actualString.Split(':')[0];
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }
            return result;
        }

        public static string GetUserIDFromToken(string token)
        {
            var result = string.Empty;
            try
            {
                byte[] tokenByte = Convert.FromBase64String(token);
                string actualString = Encoding.UTF8.GetString(tokenByte);
                if (!string.IsNullOrEmpty(actualString))
                {
                    result = actualString.Split(':')[2];
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }
            return result;
        }
        #endregion
    }
}