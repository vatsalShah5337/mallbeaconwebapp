﻿using System.Web;
using System.Web.Optimization;

namespace MallBeaconWebApp.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                "~/assets/libs/datatables.net/js/jquery.dataTables.min.js",
                "~/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js",
                "~/assets/js/pages/datatables.init.js",
                "~/assets/js/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/form").Include(
                "~/assets/js/pages/form-editor.init.js",
                "~/assets/js/pages/form-element.init.js",
                "~/assets/libs/bs-custom-file-input/bs-custom-file-input.min.js"
                ));

            bundles.Add(new StyleBundle("~/Content/form").Include(
                "~/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css"));
        }
    }
}
