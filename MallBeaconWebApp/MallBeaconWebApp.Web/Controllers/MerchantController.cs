﻿using MallBeaconWebApp.Core.DbClass.Admin;
using MallBeaconWebApp.Core.DbClass.Merchant;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbProperties.Merchant;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.DbTblViews.Merchant;
using MallBeaconWebApp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MallBeaconWebApp.Web.Controllers
{
    [SessionHandler(true)]
    public class MerchantController : Controller
    {
        public MerchantDb merchantDb;

        public MerchantController()
        {
            merchantDb = new MerchantDb(SessionHandler.conStr);
        }
        // GET: Merchant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        #region Unit
        public ActionResult UnitList()
        {
            var model = new UnitMasterView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.UnitList(userid);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured getting unit list";
            }
            return View("~/Views/Merchant/Unit/UnitList.cshtml", model);
        }

        public ActionResult AddUnit()
        {
            return View("~/Views/Merchant/Unit/UnitView.cshtml");
        }

        public ActionResult UpdateUnit(string unitId)
        {
            try
            {
                if (!string.IsNullOrEmpty(unitId))
                {
                    var result = merchantDb.GetUnitDetails(unitId);
                    if (result != null)
                    {
                        return View("~/Views/Merchant/Unit/UnitView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Unit not found";
                    }
                }
                else
                    TempData["Error"] = "Something went wrong";

            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured during getting unit";
            }
            return RedirectToAction("UnitList");
        }

        [HttpPost]
        public ActionResult AddEditUnit(UnitMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userid = string.Empty;
                    var token = Session["Merchant"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        userid = SessionHandler.GetUserIDFromToken(token);
                        if (string.IsNullOrEmpty(userid))
                            return RedirectToAction("AdminLogin", "Home");
                        else
                            input.MerchantID = Convert.ToInt64(userid);
                    }
                    if (input.ID > 0)
                    {
                        var result = merchantDb.UpdateUnit(input);
                        if (result)
                            TempData["Success"] = "Unit updated successfully";
                        else
                            TempData["Error"] = "Failed to update unit";
                    }
                    else
                    {
                        var result = merchantDb.CreateUnit(input);
                        if (result)
                            TempData["Success"] = "Unit created successfully";
                        else
                            TempData["Error"] = "Failed to create unit";
                    }
                }
                else
                    return View("~/Views/Merchant/Unit/UnitView.cshtml", input);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("UnitList");
        }

        public ActionResult DeleteUnit(string unitId)
        {
            return View();
        }
        #endregion

        #region Product Menu Management
        public ActionResult ProductMenu()
        {
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        var result = merchantDb.CheckMenuProductRights(userid);
                        if (!string.IsNullOrEmpty(result))
                        {
                            if (result == "selectcurrency")
                            {
                                TempData["Error"] = "Please Setup Currency. Then You will able to setup Product/Menu.";
                                return RedirectToAction("UserProfile", "Home");
                            }
                            else if (result.ToLower() == "product")
                                return RedirectToAction("ProductList", "Merchant");
                            else if (result.ToLower() == "menu")
                                return RedirectToAction("MenuList");
                        }
                    }
                }
                return RedirectToAction("Dashboard");
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while product or menu";
                return RedirectToAction("Dashboard");
            }
        }

        #region Menu Management
        public ActionResult MenuList()
        {
            var model = new MenuView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.ListMenu(userid);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting event list";
            }
            return View("~/Views/Merchant/Menu/MenuList.cshtml", model);
        }

        public ActionResult AddMenu()
        {
            var model = new Menu();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        var input = merchantDb.getPreMenu("", userid, "");
                        model.UnitList = input.UnitList;
                        model.CategoryList = input.CategoryList;
                        model.ModifiedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return View("~/Views/Merchant/Menu/MenuView.cshtml", model);
        }

        public ActionResult UpdateMenu(string ProductID)
        {
            try
            {
                if (!string.IsNullOrEmpty(ProductID))
                {
                    var token = Session["Merchant"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        var userid = SessionHandler.GetUserIDFromToken(token);
                        if (!string.IsNullOrEmpty(userid))
                        {
                            var result = merchantDb.getMenuDetails(ProductID);
                            var model = merchantDb.getPreMenu(result.UnitID.ToString(), userid, result.CategoryID.ToString());
                            result.UnitList = model.UnitList;
                            result.CategoryList = model.CategoryList;
                            if (result != null)
                            {
                                return View("~/Views/Merchant/Menu/MenuView.cshtml", result);
                            }
                            else
                            {
                                TempData["Error"] = "Menu not found";
                            }
                        }
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting menu details";
            }
            return RedirectToAction("MenuList");
        }

        [HttpPost]
        public ActionResult AddEditMenu(Menu input, HttpPostedFileBase ItemImage)
        {
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        if (ModelState.IsValid)
                        {
                            var oldPhotoPath = Server.MapPath("~/Upload_Files/Menu/") + input.ItemImage;
                            if (ItemImage != null)
                                input.ItemImage = ItemImage.FileName;
                            input.MerchantID = Convert.ToInt64(userid);
                            if (input.ID > 0)
                            {
                                if (ItemImage != null)
                                {
                                    //Remove old photo
                                    if (System.IO.File.Exists(oldPhotoPath))
                                    {
                                        System.IO.File.Delete(oldPhotoPath);
                                    }
                                    //Add new photo
                                    var InputFileName = Path.GetFileName(ItemImage.FileName);
                                    var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Menu/") + InputFileName);
                                    //Save file to server folder  
                                    ItemImage.SaveAs(ServerSavePath);
                                    input.ItemImage = input.ID + "_" + input.ItemImage;
                                }
                                var result = merchantDb.UpdateMenu(input);
                                if (result)
                                    TempData["Success"] = "Menu updated successfully";
                                else
                                    TempData["Error"] = "Failed to update menu";
                            }
                            else
                            {
                                if (ItemImage != null)
                                {
                                    var InputFileName = Path.GetFileName(ItemImage.FileName);
                                    var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Menu/") + InputFileName);
                                    //Save file to server folder  
                                    ItemImage.SaveAs(ServerSavePath);
                                    input.ItemImage = InputFileName;
                                }
                                var result = merchantDb.CreateMenu(input);
                                if (result)
                                    TempData["Success"] = "Menu created successfully";
                                else
                                    TempData["Error"] = "Failed to create menu";
                            }
                        }
                        else
                        {
                            var input1 = merchantDb.getPreMenu("", userid, "");
                            input.UnitList = input1.UnitList;
                            input.CategoryList = input.CategoryList;
                            input.ModifiedDate = DateTime.Today;
                            return View("~/Views/Merchant/Menu/MenuView.cshtml", input);
                        }
                    }
                }
                else
                {
                    return RedirectToAction("AdminLogin", "Home");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding menu";
            }
            return RedirectToAction("MenuList");
        }

        public ActionResult DeleteMenu(string productId)
        {
            return View();
        }
        #endregion

        #region Product Management
        public ActionResult ProductList()
        {
            return RedirectToAction("Dashboard");
        }
        #endregion
        #endregion

        #region Category Management
        public ActionResult CategoryAssignmentList()
        {
            var model = new ShopTypeCategoryMasterView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.CategoryList(userid, true, "");
                        model.Level1ParentCategory = true;
                        return View("~/Views/Merchant/Category/CategoryAssignmentList.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting category list";
            }
            return RedirectToAction("Dashboard", "Merchant");
        }

        public ActionResult ChildCategoryAssignmentList(string categoryId)
        {
            var model = new ShopTypeCategoryMasterView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.CategoryList(userid, false, categoryId);
                        model.Level1ParentCategory = false;
                        model.CategoryID = categoryId;
                        return View("~/Views/Merchant/Category/CategoryAssignmentList.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting category list";
            }
            return RedirectToAction("Dashboard", "Merchant");
        }

        public ActionResult AssignParentCategory()
        {
            //List of categories which are on Level2. parentCategoryID is not null
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        var model = merchantDb.GetPreAddCategoryAssign(true, userid, "");
                        model.IsParentCategory = true;
                        return View("~/Views/Merchant/Category/CategoryAssignmentView.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("CategoryAssignmentList", "Merchant");
        }

        public ActionResult AssignChildCategory(string categoryId, string parentCatID)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var token = Session["Merchant"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        var userid = SessionHandler.GetUserIDFromToken(token);
                        if (!string.IsNullOrEmpty(userid))
                        {
                            var result = merchantDb.AssignCategory(categoryId, userid);
                            if (result)
                                TempData["Success"] = "Category assigned successfully";
                            else
                                TempData["Error"] = "Failed to assign category";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("ChildCategoryAssignmentList", new { @categoryId = parentCatID });
        }

        [HttpPost]
        public ActionResult AssignCategory(ShopTypeCategoryMaster input)
        {
            try
            {
                if (input.ID > 0)
                {
                    var token = Session["Merchant"].ToString();
                    if (!string.IsNullOrEmpty(token))
                    {
                        var userid = SessionHandler.GetUserIDFromToken(token);
                        if (!string.IsNullOrEmpty(userid))
                        {
                            var result = merchantDb.AssignCategory(input.ID.ToString(), userid);
                            if (result)
                                TempData["Success"] = "Category assigned successfully";
                            else
                                TempData["Error"] = "Failed to assign category";
                        }
                    }
                }
                else
                {
                    if (input.IsParentCategory)
                        return RedirectToAction("AssignParentCategory");
                    else
                        return RedirectToAction("AssignChildCategory");
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            if (input.IsParentCategory)
                return RedirectToAction("CategoryAssignmentList");
            else
                return RedirectToAction("ChildCategoryAssignmentList");
        }

        public ActionResult UnAssignCategory(string categoryId)
        {
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        var result = merchantDb.UnAssignCategory(categoryId, userid);
                        if (result)
                        {
                            TempData["Success"] = "Category unassigned sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To unassigned category";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while unassigned category";
            }
            return RedirectToAction("CategoryAssignmentList", "Merchant");
        }

        public ActionResult AddChildCategory(string parentCatId)
        {
            //List of categories which are on Level2. parentCategoryID is not null
            try
            {
                var model = new ShopTypeCategoryMaster();
                model.Level1ParentCategoryId = Convert.ToInt64(parentCatId);
                return View("~/Views/Merchant/Category/ChildCategoryView.cshtml", model);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
                return RedirectToAction("ChildCategoryAssignmentList", new { @categoryId = parentCatId });
            }
        }

        [HttpPost]
        public ActionResult AddChildCategory(ShopTypeCategoryMaster input, HttpPostedFileBase Logo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Add new photo
                    if (Logo != null)
                    {
                        var InputFileName = Path.GetFileName(Logo.FileName);
                        var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Category/") + InputFileName);
                        //Save file to server folder  
                        Logo.SaveAs(ServerSavePath);
                        input.CategoryLogo = InputFileName;
                    }
                    var result = merchantDb.CreateChildCategory(input);
                    if (result)
                        TempData["Success"] = "Child category created successfully";
                    else
                        TempData["Error"] = "Failed to create child category";
                }
                else
                {
                    TempData["Error"] = "Inputs are invalid";
                    return View("~/Views/Merchant/Category/ChildCategoryView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding child category";
            }
            return RedirectToAction("ChildCategoryAssignmentList", new { categoryId = input.Level1ParentCategoryId });
        }

        public bool CheckCategoryNameExist(string catName)
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(catName))
                {
                    var res = merchantDb.CheckCategoryNameExist(catName.Trim());
                    result = res.ID > 0;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region Order Management
        public ActionResult OrderList()
        {
            var model = new OrderMasterView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.OrderList(userid);
                        return View("~/Views/Merchant/Order/OrderList.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting order list";
            }
            return RedirectToAction("Dashboard", "Merchant");
        }

        public ActionResult ViewOrder(string orderId)
        {
            var model = new OrderMaster();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid) && !string.IsNullOrEmpty(orderId))
                    {
                        model = merchantDb.GetOrderDetail(orderId, userid);
                        return View("~/Views/Merchant/Order/OrderDetailView.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting order detail";
            }
            return RedirectToAction("Dashboard", "Merchant");
        }
        #endregion

        #region Beacon Management
        public ActionResult MerchantBeaconList()
        {
            var model = new BeaconMasterView();
            try
            {
                var token = Session["Merchant"].ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    var userid = SessionHandler.GetUserIDFromToken(token);
                    if (!string.IsNullOrEmpty(userid))
                    {
                        model = merchantDb.BeaconListByMerchantID(userid);
                        return View("~/Views/Merchant/Beacon/MerchantBeaconList.cshtml", model);
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting beacon list";
            }
            return RedirectToAction("Dashboard", "Merchant");
        }
        #endregion
    }
}
