﻿using MallBeaconWebApp.Core.DbClass;
using MallBeaconWebApp.Core.DbProperties.Common;
using MallBeaconWebApp.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MallBeaconWebApp.Web.Controllers
{
    [SessionHandler(false)]
    public class HomeController : Controller
    {
        public CommonDb commonDb;
        public HomeController()
        {
            commonDb = new CommonDb(SessionHandler.conStr);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminLogin(LoginUser input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input.UserName) && !string.IsNullOrEmpty(input.Password))
                {
                    input.UserName = input.UserName.Trim();
                    //encrypt password
                    var password = input.Password;
                    var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
                    var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), salt);
                    input.Password = Convert.ToBase64String(hashedPassword);

                    var result = commonDb.CheckLoginAuth(input.UserName, input.Password);
                    if (result.IsActive)
                    {
                        if (result != null && !string.IsNullOrEmpty(result.RoleName))
                        {
                            var token = SessionHandler.GenerateToken(input.UserName, result.UserID);
                            Session[result.RoleName] = token;
                            Session["Name"] = result.Name;
                            TempData["Success"] = "Login Successfully";
                            if (result.RoleName.ToLower() == "merchant")
                            {
                                if (string.IsNullOrEmpty(result.CurrencyID))
                                {
                                    TempData["Success"] = "Login Successfully. Please Setup Your Profile & Currency For Products.";
                                    return RedirectToAction("UserProfile", "Home");
                                }
                                else
                                {
                                    return RedirectToAction("Dashboard", "Merchant");
                                }
                            }
                            if (result.RoleName.ToLower() == "admin")
                            {
                                return RedirectToAction("Dashboard", "Admin");
                            }
                            if (result.RoleName.ToLower() == "franchiesuser")
                            {
                                return RedirectToAction("Dashboard", "Franchies");
                            }
                        }
                        else
                        {
                            TempData["Error"] = "Invalid Credentials/User";
                        }
                    }
                    else
                    {
                        TempData["Error"] = "Your account is not active. Please contact to an administrator";
                    }
                }
                else
                {
                    TempData["Error"] = "Please Enter Username & Password";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while logged into system";
            }
            return View(input);
        }

        public ActionResult AccessRestrict()
        {
            return View();
        }

        public string PasswordHashed(string pass)
        {
            var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
            var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(pass), salt);
            var encPass = Convert.ToBase64String(hashedPassword);
            return encPass;
        }

        public ActionResult Logout()
        {
            SessionHandler.ClearSession();
            return RedirectToAction("AdminLogin");
        }

        #region Profile Related
        [SessionHandler(true)]
        public ActionResult UserProfile()
        {
            try
            {
                var token = string.Empty;
                var role = string.Empty;
                if (Session["Admin"] != null)
                {
                    token = Session["Admin"].ToString();
                    role = "Admin";
                }
                else if (Session["Merchant"] != null)
                {
                    token = Session["Merchant"].ToString();
                    role = "Merchant";
                }
                else if (Session["FranchiesUser"] != null)
                {
                    token = Session["FranchiesUser"].ToString();
                    role = "FranchiesUser";
                }
                if (!string.IsNullOrEmpty(token))
                {
                    var username = SessionHandler.GetUsernameFromToken(token);
                    if (!string.IsNullOrEmpty(username))
                    {
                        var result = commonDb.GetUserDetails(username);
                        if (result != null)
                        {
                            var input = commonDb.getPreUserProfile();
                            result.CurrencyName = input.CurrencyName;
                            result.RoleName = role;
                            return View(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting profile";
            }
            SessionHandler.ClearSession();
            return RedirectToAction("Login");
        }

        [HttpPost]
        [SessionHandler(true)]
        public ActionResult UserProfile(CommonUser input, HttpPostedFileBase Logo, HttpPostedFileBase BannerImg)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var oldLogo = Server.MapPath("~/Upload_Files/Merchant/") + input.Logo;
                    if (Logo != null)
                        input.Logo = Logo.FileName;
                    var oldbannerImg = Server.MapPath("~/Upload_Files/Merchant/") + input.BannerImg;
                    if (BannerImg != null)
                        input.BannerImg = BannerImg.FileName;
                    var result = commonDb.UpdateProfile(input);
                    if (result)
                    {
                        //Add new photo
                        if (Logo != null)
                        {
                            //Remove old photo
                            var oldPhotoPath = oldLogo;
                            if (System.IO.File.Exists(oldPhotoPath))
                            {
                                System.IO.File.Delete(oldPhotoPath);
                            }
                            var InputFileName = Path.GetFileName(Logo.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Merchant/") + InputFileName);
                            //Save file to server folder  
                            Logo.SaveAs(ServerSavePath);
                        }
                        if (BannerImg != null)
                        {
                            //Remove old photo
                            var oldBannerImg = oldbannerImg;
                            if (System.IO.File.Exists(oldBannerImg))
                            {
                                System.IO.File.Delete(oldBannerImg);
                            }
                            var InputFileName = Path.GetFileName(BannerImg.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Merchant/") + InputFileName);
                            //Save file to server folder  
                            BannerImg.SaveAs(ServerSavePath);
                        }

                        Session["Name"] = input.OwnerName;
                        TempData["Success"] = "Profile updated successfully";
                    }
                    else
                    {
                        TempData["Error"] = "Failed to update profile";
                    }
                }
                else
                {
                    TempData["Error"] = "Some inputs are wrong";
                    if (!string.IsNullOrEmpty(input.FloorNo))
                    {
                        //Floor list
                        string[] floorList = { "Ground Floor", "First Floor", "Second Floor", "Third Floor", "Fourth Floor", "Fifth Floor" };
                        input.FloorList = floorList.Select(x => new SelectListItem() { Text = x, Value = x, Selected = x == input.FloorNo }).ToList();
                    }
                    return View(input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while updating profile";
            }
            if (input.RoleName == "Admin")
                return RedirectToAction("Dashboard", "Admin");
            else if (input.RoleName == "Merchant")
                return RedirectToAction("Dashboard", "Merchant");
            else if (input.RoleName == "FranchiesUser")
                return RedirectToAction("Dashboard", "Franchies");
            else
                return RedirectToAction("UserProfile");
        }

        [SessionHandler(true)]
        public ActionResult ChangePassword()
        {
            var model = new LoginUser();
            if (Session["Admin"] != null)
                model.RoleName = "Admin";
            else if (Session["Merchant"] != null)
                model.RoleName = "Merchant";
            else if (Session["FranchiesUser"] != null)
                model.RoleName = "FranchiesUser";
            return View(model);
        }

        [HttpPost]
        [SessionHandler(true)]
        public ActionResult ChangePassword(LoginUser input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input.CurrentPassword) && !string.IsNullOrEmpty(input.Password) && !string.IsNullOrEmpty(input.ConfirmedPassword))
                {
                    if (input.Password == input.ConfirmedPassword)
                    {
                        var token = string.Empty;
                        if (Session["Admin"] != null)
                        {
                            token = Session["Admin"].ToString();
                        }
                        else if (Session["Merchant"] != null)
                        {
                            token = Session["Merchant"].ToString();
                        }
                        else if (Session["FranchiesUser"] != null)
                        {
                            token = Session["FranchiesUser"].ToString();
                        }
                        input.UserName = SessionHandler.GetUsernameFromToken(token);

                        //encrypt password
                        var password = input.Password;
                        var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
                        var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), salt);
                        input.Password = Convert.ToBase64String(hashedPassword);

                        var result = commonDb.UpdatePassword(input);
                        if (result)
                        {
                            TempData["Success"] = "Password updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update password";
                        }
                    }
                    else
                    {
                        TempData["Error"] = "Password and confirm password not matched";
                        var model = new CommonUser();
                        if (Session["Admin"] != null)
                            model.RoleName = "Admin";
                        else if (Session["Merchant"] != null)
                            model.RoleName = "Merchant";
                        else if (Session["FranchiesUser"] != null)
                            model.RoleName = "FranchiesUser";
                        return View(model);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(input.CurrentPassword))
                        TempData["Error"] = "Please enter current password";
                    else
                        TempData["Error"] = "Password and confirm password are required";

                    if (Session["Admin"] != null)
                        input.RoleName = "Admin";
                    else if (Session["Merchant"] != null)
                        input.RoleName = "Merchant";
                    else if (Session["FranchiesUser"] != null)
                        input.RoleName = "FranchiesUser";
                    return View(input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while updating password";
                SessionHandler.ClearSession();
                return RedirectToAction("Login");
            }
            if (Session["Admin"] != null)
            {
                return RedirectToAction("Dashboard", "Admin");
            }
            else if (Session["Merchant"] != null)
            {
                return RedirectToAction("Dashboard", "Merchant");
            }
            else if (Session["FranchiesUser"] != null)
            {
                return RedirectToAction("Dashboard", "Franchies");
            }
            return RedirectToAction("Login");
        }

        public bool CheckCurrentPassword(string password)
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    var token = string.Empty;
                    if (Session["Admin"] != null)
                    {
                        token = Session["Admin"].ToString();
                    }
                    else if (Session["Merchant"] != null)
                    {
                        token = Session["Merchant"].ToString();
                    }
                    else if (Session["FranchiesUser"] != null)
                    {
                        token = Session["FranchiesUser"].ToString();
                    }
                    var username = SessionHandler.GetUsernameFromToken(token);
                    //encrypt password
                    var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
                    var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), salt);
                    password = Convert.ToBase64String(hashedPassword);

                    var res = commonDb.CheckLoginAuth(username, password);
                    result = !string.IsNullOrEmpty(res.UserID);
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(LoginUser input)
        {
            if (!string.IsNullOrEmpty(input.UserName))
            {
                var token = SessionHandler.GenerateToken(input.UserName, null);
                var resetLink = HttpContext.Request.Url.Host + "/Home/ResetPassword?token=" + token;
                var bodyText = "We received a request to reset your password of DEALERT account. <br/>" +
                               "If you didn't request to reset your DEALERT account then ignore this email. <br/>" +
                               "Use below link to setup a new password for your DEALERT account.";
                var body = SessionHandler.PopulateBody(bodyText, "RESET YOUR PASSWORD", resetLink, null);
                SessionHandler.GeneralMailer(input.UserName, "Dealert Account Password Reset", body, true);
                TempData["Success"] = "Reset password link is sent to an email address";
            }
            else
            {
                TempData["Error"] = "Please enter registered email address";
            }
            return RedirectToAction("AdminLogin");
        }

        public ActionResult ResetPassword(string token)
        {
            var model = new LoginUser();
            model.token = token;
            return View(model);
        }

        [HttpPost]
        public ActionResult ResetPassword(LoginUser input)
        {
            try
            {
                if (!string.IsNullOrEmpty(input.Password) && !string.IsNullOrEmpty(input.ConfirmedPassword))
                {
                    input.UserName = SessionHandler.GetUsernameFromToken(input.token);

                    //encrypt password
                    var password = input.Password;
                    var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
                    var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), salt);
                    input.Password = Convert.ToBase64String(hashedPassword);

                    var result = commonDb.UpdatePassword(input);
                    if (result)
                    {
                        TempData["Success"] = "Password reset successfully";
                    }
                    else
                    {
                        TempData["Error"] = "Failed to reset password";
                    }
                }
                else
                {
                    TempData["Error"] = "password and confirm password are required";
                    return View(input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while reseting password";
                SessionHandler.ClearSession();
                return RedirectToAction("AdminLogin");
            }
            return RedirectToAction("AdminLogin");
        }
        #endregion
    }
}