﻿using MallBeaconWebApp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MallBeaconWebApp.Web.Controllers
{
    [SessionHandler(true)]
    public class FranchiesController : Controller
    {
        // GET: Franchies
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }
    }
}