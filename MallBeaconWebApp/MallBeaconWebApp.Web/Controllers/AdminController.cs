﻿using MallBeaconWebApp.Core.DbClass.Admin;
using MallBeaconWebApp.Core.DbClass.Merchant;
using MallBeaconWebApp.Core.DbProperties;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbProperties.Merchant;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.DbTblViews.Merchant;
using MallBeaconWebApp.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MallBeaconWebApp.Web.Controllers
{
    [SessionHandler(true)]
    public class AdminController : Controller
    {
        public MallDb mallDb;
        public MerchantDb merchantDb;
        public EventDb eventDb;
        public AdvertisementDb advertisementDb;

        public AdminController()
        {
            mallDb = new MallDb(SessionHandler.conStr);
            merchantDb = new MerchantDb(SessionHandler.conStr);
            eventDb = new EventDb(SessionHandler.conStr);
            advertisementDb = new AdvertisementDb(SessionHandler.conStr);
        }
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        #region Mall Actions
        public ActionResult MallList()
        {
            var result = new MallDetailsView();
            try
            {
                result = mallDb.ListMall();
            }
            catch (Exception ex)
            {
                result = new MallDetailsView();
                TempData["Error"] = "An error occured while mall listing";
            }
            return View(result);
        }

        public ActionResult MallRegister()
        {
            var model = new MallDeatils();
            return View("~/Views/Admin/MallView.cshtml", model);
        }

        public ActionResult UpdateMall(string mallId)
        {
            try
            {
                if (!string.IsNullOrEmpty(mallId))
                {
                    var result = mallDb.GetMallDetails(mallId);
                    if (result != null)
                    {
                        return View("~/Views/Admin/MallView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Mall not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting mall details";
            }
            return RedirectToAction("MallList");
        }

        [HttpPost]
        public ActionResult MallRegister(MallDeatils input, HttpPostedFileBase MallLogo, HttpPostedFileBase MallImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var oldPhotoPath = Server.MapPath("~/Upload_Files/Mall/") + input.MallLogo;
                        if (MallLogo != null)
                            input.MallLogo = MallLogo.FileName;
                        var oldBannerPath = Server.MapPath("~/Upload_Files/Mall/") + input.MallImage;
                        if (MallImage != null)
                            input.MallImage = MallImage.FileName;
                        var result = mallDb.UpdateMall(input);
                        if (result)
                        {
                            if (MallLogo != null)
                            {
                                //Remove old photo
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(MallLogo.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Mall/") + InputFileName);
                                //Save file to server folder  
                                MallLogo.SaveAs(ServerSavePath);
                            }
                            if (MallImage != null)
                            {
                                //Remove old photo
                                if (System.IO.File.Exists(oldBannerPath))
                                {
                                    System.IO.File.Delete(oldBannerPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(MallImage.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Mall/") + InputFileName);
                                //Save file to server folder  
                                MallImage.SaveAs(ServerSavePath);
                            }
                            TempData["Success"] = "Mall updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update mall";
                        }
                    }
                    else
                    {
                        if (MallLogo != null)
                        {
                            var InputFileName = Path.GetFileName(MallLogo.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Mall/") + InputFileName);
                            //Save file to server folder  
                            MallLogo.SaveAs(ServerSavePath);
                            input.MallLogo = InputFileName;
                        }
                        if (MallImage != null)
                        {
                            var InputFileName = Path.GetFileName(MallImage.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Mall/") + InputFileName);
                            //Save file to server folder  
                            MallImage.SaveAs(ServerSavePath);
                            input.MallImage = InputFileName;
                        }
                        var result = mallDb.CreateMall(input);
                        if (result)
                        {
                            TempData["Success"] = "Mall registered sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to register mall";
                        }
                    }
                }
                else
                {
                    return View("~/Views/Admin/MallView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while registering mall";
            }
            return RedirectToAction("MallList");
        }

        public ActionResult DeleteMall(string mallId)
        {
            return View();
        }
        #endregion


        #region Merchant action
        public ActionResult MerchantList()
        {
            var model = new MerchantMasterView();
            try
            {
                model = merchantDb.MerchantList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting merchant list";
            }
            return View(model);
        }

        public ActionResult AddMerchant()
        {
            var model = new MerchantMaster();
            try
            {
                model = merchantDb.getPreAddMerchant();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddMerchant(MerchantMaster input)
        {
            try
            {
                if (input.MallID > 0 && !string.IsNullOrEmpty(input.UserName) && !string.IsNullOrEmpty(input.OwnerName) && input.ShopCategoryID > 0 && !string.IsNullOrEmpty(input.ShopName))
                {
                    var password = SessionHandler.RandomString();
                    var salt = Encoding.ASCII.GetBytes(SessionHandler.HashedSalt);
                    var hashedPassword = SessionHandler.HashPasswordWithSalt(Encoding.UTF8.GetBytes(password), salt);
                    input.Password = Convert.ToBase64String(hashedPassword);

                    var result = merchantDb.CreateMerchant(input);
                    if (result)
                    {
                        var bodyText = "Your new merchant account has been registered on <b>DEALERT</b> website. <br/>" +
                                       "<span style='color:#83b512;'>Welcome to our DEALERT Community. </span><br/> <br/>" +
                                       "From now onwards, you are the merchant of <b>" + input.MallName + " Mall</b>.<br/>" +
                                       "You can logged in to our system by below credentials and access our system and manage your merchant account of DEALERT";

                        var specialText = " <b>USERNAME : " + input.UserName + "</b></br>" +
                                          " <b>PASSWORD : " + password + "</b></br>";
                        var body = SessionHandler.PopulateBody(bodyText, null, null, specialText);
                        SessionHandler.GeneralMailer(input.UserName, "Merchant Registration", body, true);
                        TempData["Success"] = "Merchant added sucessfully";
                    }
                    else
                    {
                        TempData["Error"] = "Failed to add Merchant";
                    }
                }
                else
                {
                    var model = merchantDb.getPreAddMerchant();
                    input.MallList = model.MallList;
                    input.FranchiesList = model.FranchiesList;
                    input.FloorList = model.FloorList;
                    input.ShopCategoryList = model.ShopCategoryList;
                    TempData["Error"] = "Please enter values in all fields";
                    return View(input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding Merchant";
            }
            return RedirectToAction("MerchantList");
        }

        public ActionResult UpdateMerchant(string merchantID)
        {
            try
            {
                if (!string.IsNullOrEmpty(merchantID))
                {
                    var result = merchantDb.GetMerchantDetails(merchantID);
                    var model = merchantDb.getPreAddMerchant();
                    result.MallList = model.MallList;
                    result.FranchiesList = model.FranchiesList;
                    result.ShopCategoryList = model.ShopCategoryList;
                    //result.FloorList = model.FloorList;
                    if (result != null)
                    {
                        return View(result);
                    }
                    else
                    {
                        TempData["Error"] = "Merchant not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting merchant details";
            }
            return RedirectToAction("MerchantList");
        }

        [HttpPost]
        public ActionResult UpdateMerchant(MerchantMaster input)
        {
            try
            {
                if (input.MallID > 0 && !string.IsNullOrEmpty(input.OwnerName) && input.ShopCategoryID > 0)
                {
                    var result = merchantDb.UpdateMerchant(input);
                    if (result)
                    {
                        TempData["Success"] = "Merchant updated successfully";
                    }
                    else
                    {
                        TempData["Error"] = "Failed to update merchant";
                    }
                }
                else
                {
                    var model = merchantDb.getPreAddMerchant();
                    input.FranchiesList = model.FranchiesList;
                    input.MallList = model.MallList;
                    input.FloorList = model.FloorList;
                    input.ShopCategoryList = model.ShopCategoryList;
                    TempData["Error"] = "Please enter values in all fields";
                    return View(input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while updating Merchant";
            }
            return RedirectToAction("MerchantList");
        }

        public ActionResult DeleteMerchant(string merchantId)
        {
            return View();
        }

        public bool CheckUsernameExist(string username)
        {
            var result = false;
            try
            {
                if (!string.IsNullOrEmpty(username))
                {
                    var res = merchantDb.CheckUsernameExit(username.Trim());
                    result = res.ID > 0;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        #endregion


        #region Beacon Assignment actions
        public ActionResult BeaconAssignList()
        {
            var model = new BeaconAssignView();
            try
            {
                model = merchantDb.BeaconAssignList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting beacon assigned list";
            }
            return View(model);
        }

        public ActionResult AddBeaconAssign()
        {
            var model = new BeaconAssignMaster();
            try
            {
                model = merchantDb.getPreBeaconAssign("", null);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return View("~/Views/Admin/BeaconAssignView.cshtml", model);
        }

        public ActionResult UpdateBeaconAssign(string merchantId)
        {
            try
            {
                if (!string.IsNullOrEmpty(merchantId))
                {
                    var result = merchantDb.GetBeaconAssignDetails(merchantId);
                    if (result != null)
                    {
                        return View("~/Views/Admin/BeaconAssignView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Beacon assignment not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting beacon assigned";
            }
            return RedirectToAction("BeaconAssignList");
        }

        [HttpPost]
        public ActionResult AddEditBeaconAssign(BeaconAssignMaster input)
        {
            try
            {
                if (input.beaconAssignIDs != null && input.beaconAssignIDs.Count > 0 && input.MerchantId > 0)
                {
                    if (input.ID > 0)
                    {
                        var result = merchantDb.UpdateBeaconAssign(input);
                        if (result)
                        {
                            TempData["Success"] = "Beacon assignment updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update beacon assignment";
                        }
                    }
                    else
                    {
                        var result = merchantDb.CreateBeaconAssign(input);
                        if (result)
                        {
                            TempData["Success"] = "Beacon assignment added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To assign beacon";
                        }
                    }
                }
                else
                {
                    TempData["Error"] = "Some inputs are wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while assign beacon";
            }
            return RedirectToAction("BeaconAssignList");
        }

        public ActionResult DeleteBeaconAssign(string baId)
        {
            return View();
        }
        #endregion


        #region Custom Mail
        public ActionResult MailManagement()
        {
            var model = new CustomMail();
            model.RoleList = merchantDb.GetRoleList();
            return View(model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult MailManagement(CustomMail input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var toEmail = !string.IsNullOrEmpty(input.ToEmail) ? input.ToEmail : "";
                    if (!string.IsNullOrEmpty(input.Role))
                    {
                        var emailList = merchantDb.GetEmailsbyRoleId(input.Role);
                        var emails = string.Join(",", emailList);
                        toEmail = emails;
                    }
                    if (!string.IsNullOrEmpty(toEmail))
                    {
                        SessionHandler.GeneralMailer(toEmail, input.Subject, input.Message, true);
                        TempData["Success"] = "E-Mail sent to customer successfully";
                    }
                    else
                    {
                        TempData["Error"] = "There is no any receiver";
                        input.RoleList = merchantDb.GetRoleList();
                        return View(input);
                    }
                }
                else
                {
                    TempData["Error"] = "Some inputs are invalid";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("MailManagement");
        }
        #endregion


        #region Events action
        public ActionResult EventList(string type)
        {
            var model = new EventView();
            try
            {
                model = eventDb.ListEvent(type);
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting event list";
            }
            return View("~/Views/Admin/Event/EventList.cshtml", model);
        }

        public ActionResult AddEvent(string type)
        {
            var model = new Event();
            try
            {
                var result = eventDb.getPreEvent("", "");
                model.MallList = result.MallList;
                model.CategoryList = result.CategoryList;
                model.Type = type;
                model.EventStartDate = DateTime.Today;
                model.EventEndDate = DateTime.Today;
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return View("~/Views/Admin/Event/EventView.cshtml", model);
        }

        public ActionResult UpdateEvent(string eventID, string type)
        {
            try
            {
                if (!string.IsNullOrEmpty(eventID))
                {
                    var result = eventDb.GetEventDetails(eventID);
                    result.Type = type;
                    if (result != null)
                    {
                        return View("~/Views/Admin/Event/EventView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Event not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting event details";
            }
            return RedirectToAction("EventList", new { type = type });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddEditEvent(Event input, HttpPostedFileBase InnerImg, HttpPostedFileBase OuterImg)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var oldInnerImgPath = Server.MapPath("~/Upload_Files/Event/") + input.InnerImgPath;
                        var oldOuterImgPath = Server.MapPath("~/Upload_Files/Event/") + input.OuterImgPath;
                        if (InnerImg != null)
                            input.InnerImgPath = InnerImg.FileName;
                        if (OuterImg != null)
                            input.OuterImgPath = OuterImg.FileName;
                        var result = eventDb.UpdateEvent(input);
                        if (result)
                        {
                            if (InnerImg != null)
                            {
                                //Remove old photo
                                var oldPhotoPath = oldInnerImgPath;
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(InnerImg.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Event/") + InputFileName);
                                //Save file to server folder  
                                InnerImg.SaveAs(ServerSavePath);
                                input.InnerImgPath = InputFileName;
                            }
                            if (OuterImg != null)
                            {
                                //Remove old photo
                                var oldPhotoPath = oldOuterImgPath;
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(OuterImg.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Event/") + InputFileName);
                                //Save file to server folder  
                                OuterImg.SaveAs(ServerSavePath);
                            }
                            TempData["Success"] = "Event updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update event";
                        }
                    }
                    else
                    {
                        if (InnerImg != null)
                        {
                            var InputFileName = Path.GetFileName(InnerImg.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Event/") + InputFileName);
                            //Save file to server folder  
                            InnerImg.SaveAs(ServerSavePath);
                            input.InnerImgPath = InputFileName;
                        }
                        if (OuterImg != null)
                        {
                            var InputFileName = Path.GetFileName(OuterImg.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Event/") + InputFileName);
                            //Save file to server folder  
                            OuterImg.SaveAs(ServerSavePath);
                            input.OuterImgPath = InputFileName;
                        }
                        var result = eventDb.CreateEvent(input);
                        if (result)
                        {
                            TempData["Success"] = "Event added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to add Event";
                        }
                    }
                }
                else
                {
                    var result = eventDb.getPreEvent(input.MallID.ToString(), input.CategoryID.ToString());
                    input.MallList = result.MallList;
                    input.CategoryList = result.CategoryList;
                    return View("~/Views/Admin/Event/EventView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("EventList", new { type = input.Type });
        }

        public ActionResult DeleteEvent(string eventId, string img1, string img2, string type)
        {
            try
            {
                var result = eventDb.DeleteEvent(eventId);
                if (result)
                {
                    if (!string.IsNullOrEmpty(img1))
                    {
                        //Remove old photo
                        var imagePath = Server.MapPath("~/Upload_Files/Event/") + img1;
                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);
                        }
                    }
                    if (!string.IsNullOrEmpty(img2))
                    {
                        //Remove old photo
                        var imagePath = Server.MapPath("~/Upload_Files/Event/") + img2;
                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);
                        }
                    }
                    TempData["Success"] = "Event deleted successfully";
                }
                else
                {
                    TempData["Error"] = "Failed to delete event";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("EventList", new { type = type });
        }
        #endregion


        #region Advertisement action
        public ActionResult AdvertisementList(string type)
        {
            var model = new AdvertisementView();
            try
            {
                model = advertisementDb.ListAdvertisement();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting advertisement list";
            }
            return View("~/Views/Admin/Advertisement/AdvertisementList.cshtml", model);
        }

        public ActionResult AddAdvertisement()
        {
            var model = new Advertisement();
            try
            {
                var result = advertisementDb.getPreAdvertisement("");
                model.MallList = result.MallList;
                model.MerchantList = result.MerchantList;
                model.EventStartDate = DateTime.Today;
                model.EventEndDate = DateTime.Today;
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return View("~/Views/Admin/Advertisement/AdvertisementView.cshtml", model);
        }

        public ActionResult UpdateAdvertisement(string advertisementID)
        {
            try
            {
                if (!string.IsNullOrEmpty(advertisementID))
                {
                    var result = advertisementDb.GetAdvertisementDetails(advertisementID);
                    if (result != null)
                    {
                        return View("~/Views/Admin/Advertisement/AdvertisementView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Event not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting advertisement details";
            }
            return RedirectToAction("AdvertisementList");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddEditAdvertisement(Advertisement input, HttpPostedFileBase InnerImg, HttpPostedFileBase OuterImg)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var oldInnerImgPath = Server.MapPath("~/Upload_Files/Advertisement/") + input.InnerImgPath;
                        var oldOuterImgPath = Server.MapPath("~/Upload_Files/Advertisement/") + input.OuterImgPath;
                        if (InnerImg != null)
                            input.InnerImgPath = InnerImg.FileName;
                        if (OuterImg != null)
                            input.OuterImgPath = OuterImg.FileName;
                        var result = advertisementDb.UpdateAdvertisement(input);
                        if (result)
                        {
                            if (InnerImg != null)
                            {
                                //Remove old photo
                                var oldPhotoPath = oldInnerImgPath;
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(InnerImg.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Advertisement/") + InputFileName);
                                //Save file to server folder  
                                InnerImg.SaveAs(ServerSavePath);
                                input.InnerImgPath = InputFileName;
                            }
                            if (OuterImg != null)
                            {
                                //Remove old photo
                                var oldPhotoPath = oldOuterImgPath;
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                //Add new photo
                                var InputFileName = Path.GetFileName(OuterImg.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Advertisement/") + InputFileName);
                                //Save file to server folder  
                                OuterImg.SaveAs(ServerSavePath);
                            }
                            TempData["Success"] = "Advertisement updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update advertisement";
                        }
                    }
                    else
                    {
                        if (InnerImg != null)
                        {
                            var InputFileName = Path.GetFileName(InnerImg.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Advertisement/") + InputFileName);
                            //Save file to server folder  
                            InnerImg.SaveAs(ServerSavePath);
                            input.InnerImgPath = InputFileName;
                        }
                        if (OuterImg != null)
                        {
                            var InputFileName = Path.GetFileName(OuterImg.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Advertisement/") + InputFileName);
                            //Save file to server folder  
                            OuterImg.SaveAs(ServerSavePath);
                            input.OuterImgPath = InputFileName;
                        }
                        var result = advertisementDb.CreateAdvertisement(input);
                        if (result)
                        {
                            TempData["Success"] = "Advertisement added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to add advertisement";
                        }
                    }
                }
                else
                {
                    var result = advertisementDb.getPreAdvertisement(input.MallID.ToString());
                    input.MallList = result.MallList;
                    input.MerchantList = result.MerchantList;
                    return View("~/Views/Admin/Advertisement/AdvertisementView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("AdvertisementList");
        }

        public ActionResult DeleteAdvertisement(string advertisementId, string img1, string img2)
        {
            try
            {
                var result = advertisementDb.DeleteAdvertisement(advertisementId);
                if (result)
                {
                    if (!string.IsNullOrEmpty(img1))
                    {
                        //Remove old photo
                        var imagePath = Server.MapPath("~/Upload_Files/Advertisement/") + img1;
                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);
                        }
                    }
                    if (!string.IsNullOrEmpty(img2))
                    {
                        //Remove old photo
                        var imagePath = Server.MapPath("~/Upload_Files/Advertisement/") + img2;
                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);
                        }
                    }
                    TempData["Success"] = "Advertisement deleted successfully";
                }
                else
                {
                    TempData["Error"] = "Failed to delete advertisement";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("AdvertisementList");
        }

        public JsonResult CheckAdPriority(string startDate, string endDate)
        {
            var result = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
                {
                    result = advertisementDb.CheckAdPriority(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate));
                }
            }
            catch (Exception ex)
            {
                result = new List<string>();
            }
            return Json(result,JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMerchants(string mallid)
        {
            var result = new List<SelectListItem>();
            try
            {
                if (!string.IsNullOrEmpty(mallid))
                {
                    result = advertisementDb.GetMerchantsbyMallID(mallid);
                }
            }
            catch (Exception ex)
            {
                result = new List<SelectListItem>();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}