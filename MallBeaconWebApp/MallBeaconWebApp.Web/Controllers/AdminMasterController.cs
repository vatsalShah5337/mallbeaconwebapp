﻿using MallBeaconWebApp.Core.DbClass.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MallBeaconWebApp.Web.Models;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.DbProperties.Admin;
using System.IO;

namespace MallBeaconWebApp.Web.Controllers
{
    [SessionHandler(true)]
    public class AdminMasterController : Controller
    {
        public MasterDb masterDb;

        public AdminMasterController()
        {
            masterDb = new MasterDb(SessionHandler.conStr);
        }

        // GET: AdminMaster
        public ActionResult Index()
        {
            return View();
        }

        #region shop type category actions
        public ActionResult ShopTypeCategoryList()
        {
            var model = new ShopTypeCategoryMasterView();
            try
            {
                model = masterDb.ShopTypeCategoryList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting shop type category list";
            }
            return View("~/Views/AdminMaster/ShopTypeCategory/ShopTypeCategoryList.cshtml", model);
        }

        public ActionResult AddShopTypeCategory()
        {
            var model = new ShopTypeCategoryMaster();
            try
            {
                var result = masterDb.getPreCategory("","");
                model.CategoryList = result.CategoryList;
                model.DisplayTypeList = result.DisplayTypeList;
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting shop type category list";
            }
            return View("~/Views/AdminMaster/ShopTypeCategory/ShopTypeCategoryView.cshtml",model);
        }

        public ActionResult UpdateShopTypeCategory(string shopTypeId)
        {
            try
            {
                if (!string.IsNullOrEmpty(shopTypeId))
                {
                    var result = masterDb.GetShopTypeCategoryDetails(shopTypeId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/ShopTypeCategory/ShopTypeCategoryView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Shop type category not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting shop type category";
            }
            return RedirectToAction("ShopTypeCategoryList");
        }

        [HttpPost]
        public ActionResult AddEditShopTypeCategory(ShopTypeCategoryMaster input, HttpPostedFileBase Logo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if(input.ParentCategoryID > 0)
                    {
                        input.DisplayType = "";
                    }
                    if (input.ID > 0)
                    {
                        var oldLogo = Server.MapPath("~/Upload_Files/Category/") + input.CategoryLogo;
                        if (Logo != null)
                            input.CategoryLogo = Logo.FileName;
                        var result = masterDb.UpdateShopTypeCategory(input);
                        if (result)
                        {
                            //Add new photo
                            if (Logo != null)
                            {
                                //Remove old photo
                                var oldPhotoPath = oldLogo;
                                if (System.IO.File.Exists(oldPhotoPath))
                                {
                                    System.IO.File.Delete(oldPhotoPath);
                                }
                                var InputFileName = Path.GetFileName(Logo.FileName);
                                var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Category/") + InputFileName);
                                //Save file to server folder  
                                Logo.SaveAs(ServerSavePath);
                            }
                            TempData["Success"] = "Shop type category updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update shop type category";
                        }
                    }
                    else
                    {
                        if (Logo != null)
                        {
                            var InputFileName = Path.GetFileName(Logo.FileName);
                            var ServerSavePath = Path.Combine(Server.MapPath("~/Upload_Files/Category/") + InputFileName);
                            //Save file to server folder  
                            Logo.SaveAs(ServerSavePath);
                            input.CategoryLogo = InputFileName;
                        }
                        var result = masterDb.CreateShopTypeCategory(input);
                        if (result)
                        {
                            TempData["Success"] = "Shop type category added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add shop type category";
                        }
                    }
                }
                else
                {
                    var result = masterDb.getPreCategory(input.ParentCategoryID.ToString(),input.DisplayType);
                    input.CategoryList = result.CategoryList;
                    input.DisplayTypeList = result.DisplayTypeList;
                    return View("~/Views/AdminMaster/ShopTypeCategory/ShopTypeCategoryView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding shop type category";
            }
            return RedirectToAction("ShopTypeCategoryList");
        }

        public ActionResult DeleteShopTypeCategory(string shopTypeId)
        {
            return View();
        }
        #endregion


        #region Amenity actions
        public ActionResult AmenityList()
        {
            var model = new AmenityMasterView();
            try
            {
                model = masterDb.AmenityList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting amenity list";
            }
            return View("~/Views/AdminMaster/Amenity/AmenityList.cshtml", model);
        }

        public ActionResult AddAmenity()
        {
            return View("~/Views/AdminMaster/Amenity/AmenityView.cshtml");
        }

        public ActionResult UpdateAmenity(string amenityId)
        {
            try
            {
                if (!string.IsNullOrEmpty(amenityId))
                {
                    var result = masterDb.GetAmenityDetails(amenityId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/Amenity/AmenityView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Amenity not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting amenity";
            }
            return RedirectToAction("AmenityList");
        }

        [HttpPost]
        public ActionResult AddEditAmenity(AmenityMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var result = masterDb.UpdateAmenity(input);
                        if (result)
                        {
                            TempData["Success"] = "Amenity updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update amenity";
                        }
                    }
                    else
                    {
                        var result = masterDb.CreateAmenity(input);
                        if (result)
                        {
                            TempData["Success"] = "Amenity added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add amenity";
                        }
                    }
                }
                else
                {
                    return View("~/Views/AdminMaster/Amenity/AmenityView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding amenity";
            }
            return RedirectToAction("AmenityList");
        }

        public ActionResult DeleteAmenity(string amenityId)
        {
            return View();
        }
        #endregion


        #region Beacon actions
        public ActionResult BeaconList()
        {
            var model = new BeaconMasterView();
            try
            {
                model = masterDb.BeaconList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting beacon list";
            }
            return View("~/Views/AdminMaster/Beacon/BeaconList.cshtml", model);
        }

        public ActionResult AddBeacon()
        {
            return View("~/Views/AdminMaster/Beacon/BeaconView.cshtml");
        }

        public ActionResult UpdateBeacon(string beaconId)
        {
            try
            {
                if (!string.IsNullOrEmpty(beaconId))
                {
                    var result = masterDb.GetBeaconDetails(beaconId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/Beacon/BeaconView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Beacon not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting beacon";
            }
            return RedirectToAction("BeaconList");
        }

        [HttpPost]
        public ActionResult AddEditBeacon(BeaconMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var result = masterDb.UpdateBeacon(input);
                        if (result)
                        {
                            TempData["Success"] = "Beacon updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update beacon";
                        }
                    }
                    else
                    {
                        var result = masterDb.CreateBeacon(input);
                        if (result)
                        {
                            TempData["Success"] = "Beacon added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add beacon";
                        }
                    }
                }
                else
                {
                    return View("~/Views/AdminMaster/Beacon/BeaconView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured";
            }
            return RedirectToAction("BeaconList");
        }

        public ActionResult DeleteBeacon(string beaconId)
        {
            return View();
        }
        #endregion


        #region Role actions
        public ActionResult RoleList()
        {
            var model = new RoleMasterView();
            try
            {
                model = masterDb.RoleList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting role list";
            }
            return View("~/Views/AdminMaster/Role/RoleList.cshtml", model);
        }

        public ActionResult AddRole()
        {
            return View("~/Views/AdminMaster/Role/RoleView.cshtml");
        }

        public ActionResult UpdateRole(string roleId)
        {
            try
            {
                if (!string.IsNullOrEmpty(roleId))
                {
                    var result = masterDb.GetRoleDetails(roleId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/Role/RoleView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Role not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting role";
            }
            return RedirectToAction("RoleList");
        }

        [HttpPost]
        public ActionResult AddEditRole(RoleMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var result = masterDb.UpdateRole(input);
                        if (result)
                        {
                            TempData["Success"] = "Role updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update role";
                        }
                    }
                    else
                    {
                        var result = masterDb.CreateRole(input);
                        if (result)
                        {
                            TempData["Success"] = "Role added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add role";
                        }
                    }
                }
                else
                {
                    return View("~/Views/AdminMaster/Role/RoleView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding role";
            }
            return RedirectToAction("RoleList");
        }

        public ActionResult DeleteRole(string roleId)
        {
            return View();
        }
        #endregion


        #region Franchies actions
        public ActionResult FranchiesList()
        {
            var model = new FranchiesMasterView();
            try
            {
                model = masterDb.FranchiesList();
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting franchies list";
            }
            return View("~/Views/AdminMaster/Franchies/FranchiesList.cshtml", model);
        }

        public ActionResult AddFranchies()
        {
            return View("~/Views/AdminMaster/Franchies/FranchiesView.cshtml");
        }

        public ActionResult UpdateFranchies(string franchiesId)
        {
            try
            {
                if (!string.IsNullOrEmpty(franchiesId))
                {
                    var result = masterDb.GetFranchiesDetails(franchiesId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/Franchies/FranchiesView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Franchies not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting franchies";
            }
            return RedirectToAction("FranchiesList");
        }

        [HttpPost]
        public ActionResult AddEditFranchies(FranchiesMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var result = masterDb.UpdateFranchies(input);
                        if (result)
                        {
                            TempData["Success"] = "Franchies updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update franchies";
                        }
                    }
                    else
                    {
                        var result = masterDb.CreateFranchies(input);
                        if (result)
                        {
                            TempData["Success"] = "Franchies added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add franchies";
                        }
                    }
                }
                else
                {
                    return View("~/Views/AdminMaster/Franchies/FranchiesView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding franchies";
            }
            return RedirectToAction("FranchiesList");
        }

        public ActionResult DeleteFranchies(string franchiesId)
        {
            return View();
        }
        #endregion


        #region Currency Actions
        public ActionResult CurrencyList()
        {
            var model = new CurrencyMasterView();
            try
            {
                model = masterDb.CurrencyList();
            }
            catch (Exception)
            {
                TempData["Error"] = "An error occured while getting currency list";
            }
            return View("~/Views/AdminMaster/Currency/CurrencyList.cshtml", model);
        }

        public ActionResult AddCurrency()
        {
            return View("~/Views/AdminMaster/Currency/CurrencyView.cshtml");
        }

        public ActionResult UpdateCurrency(string currencyId)
        {
            try
            {
                if (!string.IsNullOrEmpty(currencyId))
                {
                    var result = masterDb.GetCurrencyDetails(currencyId);
                    if (result != null)
                    {
                        return View("~/Views/AdminMaster/Currency/CurrencyView.cshtml", result);
                    }
                    else
                    {
                        TempData["Error"] = "Currency not found";
                    }
                }
                else
                {
                    TempData["Error"] = "Something went wrong";
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while getting Currency";
            }
            return RedirectToAction("CurrencyList");
        }

        [HttpPost]
        public ActionResult AddEditCurrency(CurrencyMaster input)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (input.ID > 0)
                    {
                        var result = masterDb.UpdateCurrency(input);
                        if (result)
                        {
                            TempData["Success"] = "Currency updated successfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed to update currency";
                        }
                    }
                    else
                    {
                        var result = masterDb.CreateCurrency(input);
                        if (result)
                        {
                            TempData["Success"] = "Currency added sucessfully";
                        }
                        else
                        {
                            TempData["Error"] = "Failed To add currency";
                        }
                    }
                }
                else
                {
                    return View("~/Views/AdminMaster/Currency/CurrencyView.cshtml", input);
                }
            }
            catch (Exception ex)
            {
                TempData["Error"] = "An error occured while adding currency";
            }
            return RedirectToAction("CurrencyList");
        }

        public ActionResult DeleteCurrency(string currencyId)
        {
            return View();
        }
        #endregion
    }
}