﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CarRentals.Core.DbConfigures
{
    public class DbProvider
    {
        public static DataSet GetDataSet(string connString, string commandText, CommandType commandType, ref List<SqlParameter> paramList)
        {
            DataSet ds = null;
            if (!string.IsNullOrEmpty(commandText))
            {
                using (var cnn = new SqlConnection(connString))
                {
                    var cmd = cnn.CreateCommand();
                    cmd.CommandText = commandText;
                    cmd.CommandType = commandType;

                    if (paramList != null && paramList.Count > 0)
                    {
                        foreach (var item in paramList)
                            cmd.Parameters.Add(item);
                    }

                    cnn.Open();

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        ds = new DataSet();
                        da.Fill(ds);
                    }

                    cmd.Dispose();
                    cnn.Close();
                }
            }
            return ds;
        }

        public static DataTable GetDataTable(string connString, string commandText, CommandType commandType, ref List<SqlParameter> paramList)
        {
            DataTable dt = null;
            if (!string.IsNullOrEmpty(commandText))
            {
                using (var cnn = new SqlConnection(connString))
                {
                    var cmd = cnn.CreateCommand();
                    cmd.CommandText = commandText;
                    cmd.CommandType = commandType;

                    if (paramList != null && paramList.Count > 0)
                    {
                        foreach (var item in paramList)
                            cmd.Parameters.Add(item);
                    }

                    cnn.Open();

                    using (var da = new SqlDataAdapter(cmd))
                    {
                        dt = new DataTable();
                        da.Fill(dt);
                    }

                    cmd.Dispose();
                    cnn.Close();
                }
            }
            return dt;
        }

        public static int ExecuteNonQuery(string connString, string commandText, CommandType commandType, ref List<SqlParameter> paramList)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(commandText))
            {
                using (var cnn = new SqlConnection(connString))
                {
                    var cmd = cnn.CreateCommand();
                    cmd.CommandText = commandText;
                    cmd.CommandType = commandType;

                    if (paramList != null && paramList.Count > 0)
                    {
                        foreach (var item in paramList)
                            cmd.Parameters.Add(item);
                    }

                    cnn.Open();

                    result = cmd.ExecuteNonQuery();

                    cmd.Dispose();
                    cnn.Close();
                }
            }
            return result;
        }

        public static object ExecuteScalar(string connString, string commandText, CommandType commandType, ref List<SqlParameter> paramList)
        {
            object result = null;
            if (!string.IsNullOrEmpty(commandText))
            {
                using (var cnn = new SqlConnection(connString))
                {
                    var cmd = cnn.CreateCommand();
                    cmd.CommandText = commandText;
                    cmd.CommandType = commandType;

                    if (paramList != null && paramList.Count > 0)
                    {
                        foreach (var item in paramList)
                            cmd.Parameters.Add(item);
                    }

                    cnn.Open();

                    result = cmd.ExecuteScalar();

                    cmd.Dispose();
                    cnn.Close();
                }
            }
            return result;
        }

        public static List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }


    }
}
