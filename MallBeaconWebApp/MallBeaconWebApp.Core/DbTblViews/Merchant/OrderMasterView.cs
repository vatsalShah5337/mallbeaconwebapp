﻿using MallBeaconWebApp.Core.DbProperties.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Merchant
{
    public class OrderMasterView
    {
        public List<OrderMaster> OrderList { get; set; }
        public OrderMasterView()
        {
            OrderList = new List<OrderMaster>();
        }
    }
}
