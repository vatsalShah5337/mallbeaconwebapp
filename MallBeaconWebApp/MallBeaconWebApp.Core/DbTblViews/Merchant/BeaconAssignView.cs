﻿using MallBeaconWebApp.Core.DbProperties.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Merchant
{
    public class BeaconAssignView
    {
        public List<BeaconAssignMaster> BeaconAssignList;
        public BeaconAssignView()
        {
            BeaconAssignList = new List<BeaconAssignMaster>();
        }
    }
}
