﻿using MallBeaconWebApp.Core.DbProperties.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Merchant
{
    public class MerchantMasterView
    {
            public List<MerchantMaster> MerchantList { get; set; }

            public MerchantMasterView()
            {
                MerchantList = new List<MerchantMaster>();
            }
    }
}
