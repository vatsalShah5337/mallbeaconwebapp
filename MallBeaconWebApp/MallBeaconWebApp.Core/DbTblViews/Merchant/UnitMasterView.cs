﻿using MallBeaconWebApp.Core.DbProperties.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Merchant
{
    public class UnitMasterView
    {
        public List<UnitMaster> UnitList { get; set; }

        public UnitMasterView()
        {
            UnitList = new List<UnitMaster>();
        }
    }
}
