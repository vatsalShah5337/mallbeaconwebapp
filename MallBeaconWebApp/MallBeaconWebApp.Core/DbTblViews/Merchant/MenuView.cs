﻿using MallBeaconWebApp.Core.DbProperties.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Merchant
{
    public class MenuView
    {
        public List<Menu> MenuList { get; set; }

        public MenuView()
        {
            MenuList = new List<Menu>();
        }
    }
}
