﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class CurrencyMasterView
    {
        public List<CurrencyMaster> CurrencyMasterList { get; set; }

        public CurrencyMasterView()
        {
            CurrencyMasterList = new List<CurrencyMaster>();
        }
    }
}
