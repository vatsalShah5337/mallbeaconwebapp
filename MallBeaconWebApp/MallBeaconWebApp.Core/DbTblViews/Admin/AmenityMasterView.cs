﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class AmenityMasterView
    {
        public List<AmenityMaster> AmenityList { get; set; }

        public AmenityMasterView()
        {
            AmenityList = new List<AmenityMaster>();
        }
    }
}
