﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class FranchiesMasterView
    {
        public List<FranchiesMaster> FranchiesList { get; set; }

        public FranchiesMasterView()
        {
            FranchiesList = new List<FranchiesMaster>();
        }
    }
}
