﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class EventView
    {
        public List<Event> EventList { get; set; }
        public string EventType { get; set; }
        public EventView()
        {
            EventList = new List<Event>();
        }
    }
}
