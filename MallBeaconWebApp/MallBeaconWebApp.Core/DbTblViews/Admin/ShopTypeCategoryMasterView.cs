﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class ShopTypeCategoryMasterView
    {
        public List<ShopTypeCategoryMaster> ShopTypeCatList { get; set; }
        public bool Level1ParentCategory { get; set; }
        public string CategoryID { get; set; }

        public ShopTypeCategoryMasterView()
        {
            ShopTypeCatList = new List<ShopTypeCategoryMaster>();
        }
    }
}
