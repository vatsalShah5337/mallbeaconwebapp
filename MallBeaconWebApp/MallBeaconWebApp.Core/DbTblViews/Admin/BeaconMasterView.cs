﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class BeaconMasterView
    {
        public List<BeaconMaster> BeaconList { get; set; }

        public BeaconMasterView()
        {
            BeaconList = new List<BeaconMaster>();
        }
    }
}
