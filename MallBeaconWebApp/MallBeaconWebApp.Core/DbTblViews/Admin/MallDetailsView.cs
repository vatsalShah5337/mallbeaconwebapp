﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class MallDetailsView
    {
        public List<MallDeatils> MallDetailsList { get; set; }
        public MallDetailsView()
        {
            MallDetailsList = new List<MallDeatils>();
        }
    }
}
