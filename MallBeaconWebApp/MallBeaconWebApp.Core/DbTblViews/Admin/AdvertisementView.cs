﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class AdvertisementView
    {
        public List<Advertisement> AdvertisementList { get; set; }
        public AdvertisementView()
        {
            AdvertisementList = new List<Advertisement>();
        }
    }
}
