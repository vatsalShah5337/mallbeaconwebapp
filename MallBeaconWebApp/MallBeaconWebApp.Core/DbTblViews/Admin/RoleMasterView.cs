﻿using MallBeaconWebApp.Core.DbProperties.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbTblViews.Admin
{
    public class RoleMasterView
    {
        public List<RoleMaster> RoleList { get; set; }

        public RoleMasterView()
        {
            RoleList = new List<RoleMaster>();
        }
    }
}
