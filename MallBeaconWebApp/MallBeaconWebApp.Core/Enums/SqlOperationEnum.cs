﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.Enums
{
    public enum SqlOperationEnum
    {
        Insert = 0,
        SelectAll = 1,
        Update = 2,
        Delete = 3,
        SelectOne = 4,
        GetByAccount = 5,
        CheckDbExist = 6,
        GetUserByLogin = 7
    }
}
