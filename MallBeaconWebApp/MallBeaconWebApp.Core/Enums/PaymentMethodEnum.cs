﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.Enums
{
    public enum PaymentMethodEnum
    {
        CashOnDelivery = 1,
        CreditCard = 2,
        DebitCard = 3
    }
}
