﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.Enums
{
    public enum OrderTypeEnum
    {
        NewOrder = 1,
        ReturnOrder = 2,
        CancelOrder = 3
    }
}
