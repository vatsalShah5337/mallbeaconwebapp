﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.Enums
{
    public enum OrderStatusEnum
    {
        Pending = 1,
        Approved = 2,
        Processing = 3,
        ReadyToShip = 4,
        Shipped = 5,
        Returned = 6,
        Cancelled = 7
    }
}
