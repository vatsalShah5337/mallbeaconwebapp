﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Common
{
    public class CommonUser
    {
        public Int64 ID { get; set; }
        public Int64 RoleID { get; set; }
        public string FloorNo { get; set; }
        public string ShopNo { get; set; }
        public string ShopName { get; set; }
        public string ShopAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string OwnerName { get; set; }
        public string OwnerContactNo { get; set; }
        public string ShopContactNo { get; set; }
        public string OwnerEmail { get; set; }
        public string RoleName { get; set; }
        public Int64 CurrencyID { get; set; }
        public string Logo { get; set; }
        public string BannerImg { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Linkedin { get; set; }
        public string Twitter { get; set; }

        public List<SelectListItem> FloorList { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public string CurrencyName  { get; set; }

        public CommonUser()
        {
            FloorList = new List<SelectListItem>();
            CurrencyList = new List<SelectListItem>();
        }
    }
}
