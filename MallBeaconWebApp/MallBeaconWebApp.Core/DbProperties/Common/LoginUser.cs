﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MallBeaconWebApp.Core.DbProperties.Common
{
    public class LoginUser
    {
        public int RoleID { get; set; }
        [Required(ErrorMessage = "Please Enter UserName")]
        public string  UserName { get; set; }

        public string Password { get; set; }
        public string CurrentPassword { get; set; }

        public string ConfirmedPassword { get; set; }


        public DateTime? LastLoginDate { get; set; }

        public string CreatedBy { get; set; }

        public string LastModifiedBy { get; set; }
       
        public DateTime? CreatedDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public bool IsHomeActive { get; set; }

        public string RoleName { get; set; }
        public string Name { get; set; }
        public string token { get; set; }
        public string UserID { get; set; }
        public string CurrencyID { get; set; }

    }
}
