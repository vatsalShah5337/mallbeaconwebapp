﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class UnitMaster
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "Please Enter Unit Name")]
        public string UnitName { get; set; }
        public Int64 MerchantID { get; set; }
    }
}
