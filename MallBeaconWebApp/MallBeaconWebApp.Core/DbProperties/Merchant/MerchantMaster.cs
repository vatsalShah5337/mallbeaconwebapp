﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using MallBeaconWebApp.Core.DbProperties.Common;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class MerchantMaster : LoginUser
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "Please Select Mall")]
        public Int64 MallID { get; set; }
        public Int64? FranchiseID { get; set; }
        [Required(ErrorMessage = "Please Select Shop Category")]
        public Int64 ShopCategoryID { get; set; }
        public string ShopNo { get; set; }
        public string ShopName { get; set; }
        public string ShopAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Required(ErrorMessage = "Please enter owner name")]
        public string OwnerName { get; set; }
        public string OwnerContactNo { get; set; }
        public string ShopContactNo { get; set; }
        public string OwnerEmail { get; set; }
        [Required(ErrorMessage = "Please select floor number")]
        public string FloorNo { get; set; }
        public string Logo { get; set; }
        public string BannerImg { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Linkedin { get; set; }
        public string Twitter { get; set; }

        public List<SelectListItem> MallList { get; set; }
        public List<SelectListItem> FranchiesList { get; set; }
        public List<SelectListItem> ShopCategoryList { get; set; }
        public List<SelectListItem> FloorList { get; set; }
        public string MallName { get; set; }

        public MerchantMaster()
        {
            MallList = new List<SelectListItem>();
            FranchiesList = new List<SelectListItem>();
            ShopCategoryList = new List<SelectListItem>();
            FloorList = new List<SelectListItem>();
        }
    }
}
