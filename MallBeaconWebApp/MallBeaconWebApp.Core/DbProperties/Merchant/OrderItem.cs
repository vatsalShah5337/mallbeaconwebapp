﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class OrderItem
    {
        public Int64 ID { get; set; }
        public Int64 OrderID { get; set; }
        public int Quantity { get; set; }
        public int OrderQuantity { get; set; }
        public string ItemName { get; set; }
        public float Price { get; set; }
        public string Unit { get; set; }
        public Int64 MerchantId { get; set; }
    }
}
