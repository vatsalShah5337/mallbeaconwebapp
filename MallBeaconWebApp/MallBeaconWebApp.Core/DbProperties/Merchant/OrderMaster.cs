﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class OrderMaster
    {
        public Int64 ID { get; set; }
        public string OrderNo { get; set; }
        public string CustomerName { get; set; }
        public int PaymentMethod { get; set; }
        public string Comment { get; set; }
        public bool IsPaymentDone { get; set; }
        public int OrderStatus { get; set; }
        public int OrderType { get; set; }
        public Int64 CustomerID { get; set; }
        public Int64 OfferID { get; set; }
        public Int64 DiscountID { get; set; }
        public Int64 DeliveryAddressID { get; set; }
        public Int64 BillingAddressID { get; set; }
        public float TotalAmount { get; set; }
        public DateTime OrderDate { get; set; }

        public List<OrderItem> OrderItemList { get; set; }

        //Billing Address Details
        public string BillingName { get; set; }
        public string BillingContactNo { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string BillingZipCode { get; set; }

        //Delivery Address Details
        public string DeliveryName { get; set; }
        public string DeliveryContactNo { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryCity { get; set; }
        public string DeliveryState { get; set; }
        public string DeliveryCountry { get; set; }
        public string DeliveryZipCode { get; set; }
        public bool IsDelivery { get; set; }

        public OrderMaster()
        {
            OrderItemList = new List<OrderItem>();
        }
    }
}
