﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class Menu
    {
        public Int64 ID { get; set; }
        [Required(ErrorMessage = "Please Select Category")]
        public Int64 CategoryID { get; set; }
        [Required(ErrorMessage = "Please Select Merchant")]
        public Int64 MerchantID { get; set; }
        [Required(ErrorMessage = "Please Enter Item Code")]
        public string ItemCode { get; set; }
        [Required(ErrorMessage = "Please Enter Item Name")]
        public string ItemName { get; set; }
        [Required(ErrorMessage = "Please Enter Item Price")]
        public decimal Price { get; set; }
        [Required(ErrorMessage = "Please Select Item Unit")]
        public Int64 UnitID { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Please Enter Item Quantity")]
        public int Quantity { get; set; }
        public string CreatedBy { get; set; }
        public string FoodType { get; set; }
        public string ItemImage { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> UnitList { get; set; }
        public List<SelectListItem> MerchantList { get; set; }

        public Menu()
        {
            CategoryList = new List<SelectListItem>();
            UnitList = new List<SelectListItem>();
            MerchantList = new List<SelectListItem>();
        }
    }
}
