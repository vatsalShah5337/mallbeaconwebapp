﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Merchant
{
    public class BeaconAssignMaster
    {
        public Int64 ID { get; set; }
        [Required(ErrorMessage="Please Select Beacon")]
        public Int64 BeaconId { get; set; }
        [Required(ErrorMessage = "Please Select Merchant")]
        public Int64 MerchantId { get; set; }

        public List<SelectListItem> MerchantList;
        public List<SelectListItem> BeaconList;
        public string MerchantName { get; set; }
        public string BeaconMacAddress { get; set; }
        public List<int> beaconAssignIDs { get; set; }
        public string BeaconCount { get; set; }

        public BeaconAssignMaster()
        {
            MerchantList = new List<SelectListItem>();
            BeaconList = new List<SelectListItem>();
        }
    }
}
