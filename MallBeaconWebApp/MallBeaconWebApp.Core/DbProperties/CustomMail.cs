﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties
{
    public class CustomMail
    {
        [Required(ErrorMessage = "Please Enter Subject")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Please Enter Message")]
        public string Message { get; set; }
        public string ToEmail { get; set; }
        public string Role { get; set; }
        public List<SelectListItem> RoleList;

        public CustomMail()
        {
            RoleList = new List<SelectListItem>();
        }
    }
}
