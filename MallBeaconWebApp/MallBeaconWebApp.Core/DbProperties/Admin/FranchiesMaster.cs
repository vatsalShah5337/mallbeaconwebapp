﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class FranchiesMaster
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "Enter Franchies Name")]
        public string FranchiesName { get; set; }
    }
}
