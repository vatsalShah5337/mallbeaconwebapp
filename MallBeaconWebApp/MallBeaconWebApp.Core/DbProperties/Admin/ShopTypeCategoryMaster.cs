﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class ShopTypeCategoryMaster
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage="Please enter category name")]
        public string CategoryName { get; set; }
        public Int64? ParentCategoryID { get; set; }
        public Int64? Level1ParentCategoryId { get; set; }
        public string CategoryLogo { get; set; }
        public bool IsHomeVisible { get; set; }
        public string DisplayType { get; set; }


        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> DisplayTypeList { get; set; }
        public bool IsParentCategory { get; set; }
        public bool IsAssigned { get; set; }

        public ShopTypeCategoryMaster()
        {
            CategoryList = new List<SelectListItem>();
            DisplayTypeList = new List<SelectListItem>();
        }
    }
}
