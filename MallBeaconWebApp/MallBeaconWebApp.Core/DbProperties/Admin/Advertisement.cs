﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class Advertisement
    {
        public Int64 ID { get; set; }
        [Required(ErrorMessage = "Please Select Mall")]
        public Int64 MallID { get; set; }
        [Required(ErrorMessage = "Please Enter Event Title")]
        public string Title { get; set; }
        public string OuterImgPath { get; set; }
        public string InnerImgPath { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Please Select Event Start Date")]
        public DateTime EventStartDate { get; set; }
        [Required(ErrorMessage = "Please Select Event End Date")]
        public DateTime EventEndDate { get; set; }
        public string MallName { get; set; }
        public List<SelectListItem> MallList { get; set; }
        public Int64? MerchantID { get; set; }
        public List<SelectListItem> MerchantList { get; set; }
        public string Type { get; set; }
        public int Priority { get; set; }
        public List<SelectListItem> PriorityList { get; set; }

        public Advertisement()
        {
            MerchantList = new List<SelectListItem>();
            MallList = new List<SelectListItem>();
            PriorityList = new List<SelectListItem>();
        }

    }
}
