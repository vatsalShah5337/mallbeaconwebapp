﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class CurrencyMaster
    {
        public Int64 ID { get; set; }
        //[Required(ErrorMessage ="Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter short name")]
        public string ShortName { get; set; }
        [Required(ErrorMessage = "Please enter symbol")]
        public string Symbol { get; set; }
    }
}
