﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class MallDeatils
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage="Please Enter Mall Name")]
        public string MallName { get; set; }
        public float LocationLat { get; set; }
        public float LocationLong { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string OwnerName { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerContactNo { get; set; }
        public string MallArea { get; set; }
        public string ShopLeaseDuration { get; set; }
        public int TotalNoShops { get; set; }
        public string BuilderGroupName { get; set; }
        public string MallWebsite { get; set; }
        public string ParkingSpace { get; set; }
        public string MallLogo { get; set; }
        public string MallImage { get; set; }
        public bool IsActive { get; set; }
    }
}
