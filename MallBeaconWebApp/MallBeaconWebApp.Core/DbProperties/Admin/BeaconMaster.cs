﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class BeaconMaster
    {
        public Int64 ID { get; set; }

        [Required(ErrorMessage = "Please Enter Beacon Mac Address")]
        public string MacAddress { get; set; }
    }
}
