﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbProperties.Admin
{
    public class Event
    {
        public Int64 ID { get; set; }
        [Required(ErrorMessage = "Please Select Mall")]
        public Int64 MallID { get; set; }
        [Required(ErrorMessage="Please Enter Event Title")]
        public string Title { get; set; }
        public string OuterImgPath { get; set; }
        public string InnerImgPath { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Please Select Event Start Date")]
        public DateTime EventStartDate { get; set; }
        [Required(ErrorMessage = "Please Select Event End Date")]
        public DateTime EventEndDate { get; set; }
        public string MallName { get; set; }
        public List<SelectListItem> MallList { get; set; }
        [Required(ErrorMessage = "Please Select Category")]
        public Int64 CategoryID { get; set; }
        public string CategoryName { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public string Type { get; set; }

        public Event()
        {
            MallList = new List<SelectListItem>();
            CategoryList = new List<SelectListItem>();
        }

    }
}
