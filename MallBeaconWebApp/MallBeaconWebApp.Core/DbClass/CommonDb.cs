﻿using CarRentals.Core.DbConfigures;
using MallBeaconWebApp.Core.DbProperties.Common;
using MallBeaconWebApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbClass
{
    public class CommonDb
    {
        public string connectionString = string.Empty;
        public CommonDb(string conStr)
        {
            connectionString = conStr;
        }

        public LoginUser CheckLoginAuth(string username, string password)
        {
            var result = new LoginUser();
            try
            {
                var cmdText = "CommonMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@Username", Value = username });
                parameterList.Add(new SqlParameter { ParameterName = "@Password", Value = password });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    result.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
                    result.UserID = dt.Rows[0]["ID"].ToString();
                    result.RoleName = dt.Rows[0]["RoleName"].ToString();
                    result.Name = dt.Rows[0]["OwnerName"].ToString();
                    result.CurrencyID = dt.Rows[0]["CurrencyID"].ToString();
                }
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public CommonUser GetUserDetails(string username)
        {
            var result = new CommonUser();
            try
            {
                var cmdText = "CommonMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@Username", Value = username });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "ProfileData" });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    result.ID = Convert.ToInt64(dt.Rows[0]["ID"]);
                    result.RoleID = Convert.ToInt64(dt.Rows[0]["RoleID"]);
                    result.FloorNo = dt.Rows[0]["FloorNo"].ToString();
                    result.ShopNo = dt.Rows[0]["ShopNo"].ToString();
                    result.ShopName = dt.Rows[0]["ShopName"].ToString();
                    result.ShopAddress = dt.Rows[0]["ShopAddress"].ToString();
                    result.City = dt.Rows[0]["City"].ToString();
                    result.State = dt.Rows[0]["State"].ToString();
                    result.OwnerName = dt.Rows[0]["OwnerName"].ToString();
                    result.OwnerContactNo = dt.Rows[0]["OwnerContactNo"].ToString();
                    result.ShopContactNo = dt.Rows[0]["ShopContactNo"].ToString();
                    result.OwnerEmail = dt.Rows[0]["OwnerEmail"].ToString();
                    result.Logo = dt.Rows[0]["Logo"].ToString();
                    result.BannerImg = dt.Rows[0]["BannerImg"].ToString();
                    result.Facebook = dt.Rows[0]["Facebook"].ToString();
                    result.Instagram = dt.Rows[0]["Instagram"].ToString();
                    result.Linkedin = dt.Rows[0]["Linkedin"].ToString();
                    result.Twitter = dt.Rows[0]["Twitter"].ToString();
                    if (dt.Rows[0]["CurrencyID"] != DBNull.Value)
                        result.CurrencyID = Convert.ToInt64(dt.Rows[0]["CurrencyID"]);
                    //Floor list
                    string[] floorList = { "Ground Floor", "First Floor", "Second Floor", "Third Floor", "Fourth Floor", "Fifth Floor" };
                    result.FloorList = floorList.Select(x => new SelectListItem() { Text = x, Value = x, Selected = !string.IsNullOrEmpty(result.FloorNo) ? x == result.FloorNo : false }).ToList();
                }
            }
            catch (Exception ex)
            {
                result = null;
            }
            return result;
        }

        public bool UpdateProfile(CommonUser input)
        {
            var result = false;
            try
            {
                var cmdText = "CommonMasterCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@FloorNo", Value = input.FloorNo });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopNo", Value = input.ShopNo });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopName", Value = input.ShopName });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopAddress", Value = input.ShopAddress });
                parameterList.Add(new SqlParameter { ParameterName = "@City", Value = input.City });
                parameterList.Add(new SqlParameter { ParameterName = "@State", Value = input.State });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerName", Value = input.OwnerName });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerContactNo", Value = input.OwnerContactNo });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopContactNo", Value = input.ShopContactNo });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerEmail", Value = input.OwnerEmail });
                parameterList.Add(new SqlParameter { ParameterName = "@CurrencyID", Value = 1 });
                parameterList.Add(new SqlParameter { ParameterName = "@Logo", Value = input.Logo });
                parameterList.Add(new SqlParameter { ParameterName = "@BannerImg", Value = input.BannerImg });
                parameterList.Add(new SqlParameter { ParameterName = "@Facebook", Value = input.Facebook });
                parameterList.Add(new SqlParameter { ParameterName = "@Instagram", Value = input.Instagram });
                parameterList.Add(new SqlParameter { ParameterName = "@Linkedin", Value = input.Linkedin });
                parameterList.Add(new SqlParameter { ParameterName = "@Twitter", Value = input.Twitter });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool UpdatePassword(LoginUser input)
        {
            var result = false;
            try
            {
                var cmdText = "CommonMasterCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@UserName", Value = input.UserName });
                parameterList.Add(new SqlParameter { ParameterName = "@Password", Value = input.Password });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "UpdatePassword" });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public CommonUser getPreUserProfile()
        {
            var model = new CommonUser();
            try
            {
                //Currency dropdown list
                //var cmdText1 = "Select ID as CurrencyId, Symbol + ' - ' + ShortName as CurrencyName from CurrencyMaster";
                //var parameterList = new List<SqlParameter>();
                //var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                //if (dt1 != null && dt1.Rows.Count > 0)
                //{
                //    model.CurrencyList = (from DataRow dr in dt1.Rows
                //                          select new SelectListItem()
                //                          {
                //                              Value = dr["CurrencyId"].ToString(),
                //                              Text = dr["CurrencyName"].ToString(),
                //                          }).ToList();
                //    model.CurrencyList.Insert(0, new SelectListItem()
                //    {
                //        Value = "",
                //        Text = "Select Currency",
                //    });
                //}

                //Get currency name
                var cmdText1 = "Select Symbol + ' - ' + ShortName as CurrencyName from CurrencyMaster where ID = 1";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.CurrencyName = dt1.Rows[0]["CurrencyName"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
    }
}
