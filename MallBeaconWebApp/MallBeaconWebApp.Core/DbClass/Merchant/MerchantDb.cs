﻿using MallBeaconWebApp.Core.DbTblViews.Merchant;
using System.Data.SqlClient;
using CarRentals.Core.DbConfigures;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MallBeaconWebApp.Core.Enums;
using System.Data;
using MallBeaconWebApp.Core.DbProperties.Merchant;
using MallBeaconWebApp.Core.DbProperties;
using System.Web.Mvc;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.DbProperties.Admin;

namespace MallBeaconWebApp.Core.DbClass.Merchant
{
    public class MerchantDb
    {
        public string connectionString = string.Empty;
        public MerchantDb(string conStr)
        {
            connectionString = conStr;
        }

        #region Merchant Managment
        public MerchantMasterView MerchantList()
        {
            var model = new MerchantMasterView();
            try
            {
                var cmdText = "MerchantMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.MerchantList = (from DataRow dr in dt.Rows
                                          select new MerchantMaster()
                                          {
                                              ID = Convert.ToInt64(dr["ID"]),
                                              MallID = Convert.ToInt64(dr["MallID"]),
                                              UserName = dr["UserName"].ToString(),
                                              ShopNo = dr["ShopNo"].ToString(),
                                              ShopName = dr["ShopName"].ToString(),
                                              ShopAddress = dr["ShopAddress"].ToString(),
                                              City = dr["City"].ToString(),
                                              State = dr["State"].ToString(),
                                              OwnerName = dr["OwnerName"].ToString(),
                                              OwnerContactNo = dr["OwnerContactNo"].ToString(),
                                              ShopContactNo = dr["ShopContactNo"].ToString(),
                                              MallName = dr["MallName"].ToString()
                                          }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public MerchantMaster GetMerchantDetails(string MerchantID)
        {
            var model = new MerchantMaster();
            try
            {
                var cmdText = "MerchantMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = Convert.ToInt64(MerchantID) });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.UserName = dt.Rows[0]["UserName"].ToString();
                    model.RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"].ToString());
                    model.FranchiseID = !string.IsNullOrEmpty(dt.Rows[0]["FranchiseID"].ToString()) ? Convert.ToInt64(dt.Rows[0]["FranchiseID"].ToString()) : (Int64?)null;
                    model.MallID = Convert.ToInt64(dt.Rows[0]["MallID"].ToString());
                    model.ShopNo = dt.Rows[0]["ShopNo"].ToString();
                    model.ShopName = dt.Rows[0]["ShopName"].ToString();
                    model.ShopAddress = dt.Rows[0]["ShopAddress"].ToString();
                    model.City = dt.Rows[0]["City"].ToString();
                    model.State = dt.Rows[0]["State"].ToString();
                    model.OwnerName = dt.Rows[0]["OwnerName"].ToString();
                    model.OwnerEmail = dt.Rows[0]["OwnerEmail"].ToString();
                    model.OwnerContactNo = dt.Rows[0]["OwnerContactNo"].ToString();
                    model.ShopContactNo = dt.Rows[0]["ShopContactNo"].ToString();
                    model.IsActive = bool.Parse(dt.Rows[0]["IsActive"].ToString());
                    model.IsHomeActive = bool.Parse(dt.Rows[0]["IsHomeActive"].ToString());
                    model.ShopCategoryID = Convert.ToInt64(dt.Rows[0]["ShopCategoryID"].ToString());
                    model.FloorNo = dt.Rows[0]["FloorNo"].ToString();
                    model.Logo = dt.Rows[0]["Logo"].ToString();
                    model.BannerImg = dt.Rows[0]["BannerImg"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateMerchant(MerchantMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "MerchantMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@UserName", Value = input.UserName });
                parameterList.Add(new SqlParameter { ParameterName = "@Password", Value = input.Password });
                parameterList.Add(new SqlParameter { ParameterName = "@RoleID", Value = 2 }); //Pass hard coded bcz merchant role id 2 and roles are static
                parameterList.Add(new SqlParameter { ParameterName = "@FranchiseID", Value = input.FranchiseID });
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerName", Value = input.OwnerName });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerEmail", Value = input.OwnerEmail });
                parameterList.Add(new SqlParameter { ParameterName = "@IsActive", Value = input.IsActive });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopCategoryID", Value = input.ShopCategoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopName", Value = input.ShopName });
                parameterList.Add(new SqlParameter { ParameterName = "@IsHomeActive", Value = input.IsHomeActive });
                //CurrencyID set staticly
                parameterList.Add(new SqlParameter { ParameterName = "@CurrencyID", Value = 1 });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool UpdateMerchant(MerchantMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "MerchantMasterCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@RoleID", Value = 2 }); //Pass hard coded bcz merchant role id 2 and roles are static
                parameterList.Add(new SqlParameter { ParameterName = "@FranchiseID", Value = input.FranchiseID });
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopCategoryID", Value = input.ShopCategoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerName", Value = input.OwnerName });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerEmail", Value = input.OwnerEmail });
                parameterList.Add(new SqlParameter { ParameterName = "@IsActive", Value = input.IsActive });
                parameterList.Add(new SqlParameter { ParameterName = "@IsHomeActive", Value = input.IsHomeActive });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopName", Value = input.ShopName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public MerchantMaster getPreAddMerchant()
        {
            var model = new MerchantMaster();
            try
            {
                //Mall dropdown list
                var cmdText1 = "Select ID as mallId, MallName from MallMaster";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.MallList = (from DataRow dr in dt1.Rows
                                      select new SelectListItem()
                                      {
                                          Value = dr["mallId"].ToString(),
                                          Text = dr["MallName"].ToString(),
                                      }).ToList();
                    model.MallList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Mall",
                    });
                }

                //Franchies dropdown list
                var cmdText2 = "Select ID, FranchiesName from FranchiesMaster";
                var dt2 = DbProvider.GetDataTable(connectionString, cmdText2, CommandType.Text, ref parameterList);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    model.FranchiesList = (from DataRow dr in dt2.Rows
                                           select new SelectListItem()
                                           {
                                               Value = dr["ID"].ToString(),
                                               Text = dr["FranchiesName"].ToString(),
                                           }).ToList();
                    model.FranchiesList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Franchies",
                    });
                }

                //Categoy dropdown list
                var cmdText3 = "Select ID, CategoryName from ShopTypeCategoryMaster where ParentCategoryID IS NULL and Level1ParentCategoryID IS NULL";
                var dt3 = DbProvider.GetDataTable(connectionString, cmdText3, CommandType.Text, ref parameterList);
                if (dt3 != null && dt3.Rows.Count > 0)
                {
                    model.ShopCategoryList = (from DataRow dr in dt3.Rows
                                              select new SelectListItem()
                                              {
                                                  Value = dr["ID"].ToString(),
                                                  Text = dr["CategoryName"].ToString(),
                                              }).ToList();
                    model.ShopCategoryList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Shop Category",
                    });
                }

                //Floor list
                string[] floorList = { "Ground Floor", "First Floor", "Second Floor", "Third Floor", "Fourth Floor", "Fifth Floor" };
                model.FloorList = floorList.Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public MerchantMaster CheckUsernameExit(string username)
        {
            var model = new MerchantMaster();
            try
            {
                var cmdText = "MerchantMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@UserName", Value = username });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "CheckUserExist" });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Beacon Assignment Managment
        public BeaconAssignView BeaconAssignList()
        {
            var model = new BeaconAssignView();
            try
            {
                var cmdText = "BeaconAssignCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.BeaconAssignList = (from DataRow dr in dt.Rows
                                              select new BeaconAssignMaster()
                                              {
                                                  MerchantId = Convert.ToInt64(dr["MerchantId"]),
                                                  MerchantName = dr["ShopName"].ToString(),
                                                  BeaconCount = dr["BeaconCount"].ToString(),
                                              }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public BeaconAssignMaster GetBeaconAssignDetails(string merchantId)
        {
            var model = new BeaconAssignMaster();
            try
            {
                var cmdText = "BeaconAssignCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantId", Value = Convert.ToInt64(merchantId) });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.MerchantId = Convert.ToInt64(dt.Rows[0]["MerchantId"].ToString());
                    var beaconIdList = (from dr in dt.AsEnumerable() select dr["BeaconId"].ToString()).ToList();
                    var result = getPreBeaconAssign(model.MerchantId.ToString(), beaconIdList);
                    model.MerchantList = result.MerchantList;
                    model.BeaconList = result.BeaconList;
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateBeaconAssign(BeaconAssignMaster input)
        {
            var result = false;
            try
            {
                var valuesList = input.beaconAssignIDs.Select(z => "(" + z + "," + input.MerchantId + ")").ToList();
                var values = string.Join(", ", valuesList);
                var cmdText = "INSERT INTO BeaconAssignMaster (BeaconId, MerchantId) values " + values;

                var parameterList = new List<SqlParameter>();
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool UpdateBeaconAssign(BeaconAssignMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "BeaconAssignCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantId", Value = input.MerchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Delete.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    var valuesList = input.beaconAssignIDs.Select(z => "(" + z + "," + input.MerchantId + ")").ToList();
                    var values = string.Join(", ", valuesList);
                    var cmdText1 = "INSERT INTO BeaconAssignMaster (BeaconId, MerchantId) values " + values;
                    var parameterList1 = new List<SqlParameter>();
                    var dbOutput1 = DbProvider.ExecuteNonQuery(connectionString, cmdText1, CommandType.Text, ref parameterList1);
                    if (dbOutput1 > 0)
                    {
                        result = true;
                    }
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public BeaconAssignMaster getPreBeaconAssign(string merchantId, List<string> beaconList)
        {
            var model = new BeaconAssignMaster();
            try
            {
                var cmdText = string.Empty;
                //Role dropdown list
                cmdText = "select * from BeaconMaster where ID NOT IN (select BeaconId from BeaconAssignMaster)";
                if (!string.IsNullOrEmpty(merchantId))
                    cmdText = "select * from BeaconMaster where ID NOT IN (select BeaconId from BeaconAssignMaster where MerchantId != " + merchantId + ")";
                var parameterList = new List<SqlParameter>();
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.BeaconList = (from DataRow dr in dt.Rows
                                        select new SelectListItem()
                                        {
                                            Value = dr["ID"].ToString(),
                                            Text = dr["MacAddress"].ToString(),
                                            Selected = beaconList != null && beaconList.Count > 0 ? beaconList.Any(z => z == dr["ID"].ToString()) : false
                                        }).ToList();
                }

                //Mall dropdown list
                var cmdText1 = "Select UM.ID, UM.ShopName from UserMaster as UM inner join RoleMaster as RM ON UM.RoleID = RM.ID where RM.RoleName = 'Merchant'";
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.MerchantList = (from DataRow dr in dt1.Rows
                                          select new SelectListItem()
                                          {
                                              Value = dr["ID"].ToString(),
                                              Text = dr["ShopName"].ToString(),
                                              Selected = !string.IsNullOrEmpty(merchantId) ? dr["ID"].ToString() == merchantId : false
                                          }).ToList();
                    model.MerchantList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Merchant",
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public BeaconMasterView BeaconListByMerchantID(string merchantId)
        {
            var model = new BeaconMasterView();
            try
            {
                var cmdText = "BeaconAssignCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantId", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "BeaconListByMerchant" });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.BeaconList = (from DataRow dr in dt.Rows
                                        select new BeaconMaster()
                                        {
                                            ID = Convert.ToInt64(dr["ID"]),
                                            MacAddress = dr["MacAddress"].ToString()
                                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Roles
        public List<SelectListItem> GetRoleList()
        {
            var model = new List<SelectListItem>();
            try
            {
                var cmdText = "Select * from RoleMaster";

                var parameterList = new List<SqlParameter>();

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model = (from DataRow dr in dt.Rows
                             select new SelectListItem()
                             {
                                 Value = dr["ID"].ToString(),
                                 Text = dr["RoleName"].ToString()
                             }).ToList();
                    model.Insert(0, new SelectListItem() { Text = "Select Role", Value = "" });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public List<string> GetEmailsbyRoleId(string roleId)
        {
            var model = new List<string>();
            try
            {
                var cmdText = "select um.UserName from UserMaster as um inner join RoleMaster as rm on um.RoleID = rm.ID where rm.ID = '" + roleId + "'";

                var parameterList = new List<SqlParameter>();

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model = (from DataRow dr in dt.Rows
                             select dr["UserName"].ToString()).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Unit
        public bool CreateUnit(UnitMaster input)
        {
            var result = true;
            try
            {
                var cmdText = "UnitMasterCRUD";
                var ParameterList = new List<SqlParameter>();
                ParameterList.Add(new SqlParameter { ParameterName = "@UnitName", Value = input.UnitName });
                ParameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });
                ParameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref ParameterList);
                if (dbOutput > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public bool UpdateUnit(UnitMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "UnitMasterCRUD";
                var ParameterList = new List<SqlParameter>();
                ParameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                ParameterList.Add(new SqlParameter { ParameterName = "@UnitName", Value = input.UnitName });
                ParameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });
                ParameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref ParameterList);
                if (dbOutput > 0)
                    result = true;
                else
                    result = false;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public UnitMasterView UnitList(string merchantId)
        {
            var model = new UnitMasterView();
            try
            {
                var cmdText = "UnitMasterCRUD";
                var ParameterList = new List<SqlParameter>();
                ParameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantId });
                ParameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref ParameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.UnitList = (from DataRow dr in dt.Rows
                                      select new UnitMaster()
                                      {
                                          ID = Convert.ToInt64(dr["ID"]),
                                          UnitName = dr["UnitName"].ToString()
                                      }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public UnitMaster GetUnitDetails(string unitId)
        {
            var model = new UnitMaster();
            try
            {
                var cmdText = "UnitMasterCRUD";
                var ParameterList = new List<SqlParameter>();
                ParameterList.Add(new SqlParameter { ParameterName = "@ID", Value = Convert.ToInt64(unitId) });
                ParameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref ParameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.UnitName = dt.Rows[0]["UnitName"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Check Product/Menu
        public string CheckMenuProductRights(string merchantId)
        {
            var type = string.Empty;
            try
            {
                var parameterList = new List<SqlParameter>();
                var cmdText = "select stm.DisplayType, um.CurrencyID from UserMaster as um inner join ShopTypeCategoryMaster as stm on um.ShopCategoryID = stm.ID where um.ID ='" + merchantId + "'";
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var currencyID = dt.Rows[0]["CurrencyID"].ToString();
                    if (!string.IsNullOrEmpty(currencyID))
                        type = dt.Rows[0]["DisplayType"].ToString();
                    else
                        type = "selectcurrency";
                }
            }
            catch (Exception ex)
            {
                type = string.Empty;
            }
            return type;
        }
        #endregion

        #region Menu
        public bool CreateMenu(Menu input)
        {
            var status = true;
            try
            {
                var cmdText = "MenuCRUD";
                var parameterlist = new List<SqlParameter>();
                parameterlist.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });
                parameterlist.Add(new SqlParameter { ParameterName = "@CategoryID", Value = input.CategoryID });
                parameterlist.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemCode", Value = input.ItemCode });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemName", Value = input.ItemName });
                parameterlist.Add(new SqlParameter { ParameterName = "@Price", Value = input.Price });
                parameterlist.Add(new SqlParameter { ParameterName = "@UnitID", Value = input.UnitID });
                parameterlist.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterlist.Add(new SqlParameter { ParameterName = "@Quantity", Value = input.Quantity });
                parameterlist.Add(new SqlParameter { ParameterName = "@CreatedBy", Value = input.MerchantID });
                parameterlist.Add(new SqlParameter { ParameterName = "@FoodType", Value = input.FoodType });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemImage", Value = input.ItemImage });
                parameterlist.Add(new SqlParameter { ParameterName = "@ModifiedDate", Value = DateTime.Now });
                var result = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterlist);
                if (result > 0)
                    status = true;
                else
                    status = false;
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public bool UpdateMenu(Menu input)
        {
            var status = true;
            try
            {
                var cmdText = "MenuCRUD";
                var parameterlist = new List<SqlParameter>();
                parameterlist.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });
                parameterlist.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterlist.Add(new SqlParameter { ParameterName = "@CategoryID", Value = input.CategoryID });
                parameterlist.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemCode", Value = input.ItemCode });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemName", Value = input.ItemName });
                parameterlist.Add(new SqlParameter { ParameterName = "@Price", Value = input.Price });
                parameterlist.Add(new SqlParameter { ParameterName = "@UnitID", Value = input.UnitID });
                parameterlist.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterlist.Add(new SqlParameter { ParameterName = "@Quantity", Value = input.Quantity });
                parameterlist.Add(new SqlParameter { ParameterName = "@CreatedBy", Value = input.MerchantID });
                parameterlist.Add(new SqlParameter { ParameterName = "@FoodType", Value = input.FoodType });
                parameterlist.Add(new SqlParameter { ParameterName = "@ItemImage", Value = input.ItemImage });
                parameterlist.Add(new SqlParameter { ParameterName = "@ModifiedDate", Value = DateTime.Now });
                var result = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterlist);
                if (result > 0)
                    status = true;
                else
                    status = false;
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public MenuView ListMenu(string merchantId)
        {
            var model = new MenuView();
            try
            {
                var cmdText = "MenuCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);

                if (dt != null && dt.Rows.Count > 0)
                {
                    model.MenuList = (from DataRow dr in dt.Rows
                                      select new Menu()
                                      {
                                          ID = Convert.ToInt64(dr["ID"]),
                                          CategoryID = Convert.ToInt64(dr["CategoryID"]),
                                          MerchantID = Convert.ToInt64(dr["MerchantID"]),
                                          ItemCode = dr["ItemCode"].ToString(),
                                          ItemName = dr["ItemName"].ToString(),
                                          Price = Convert.ToDecimal(dr["Price"]),
                                          UnitID = Convert.ToInt64(dr["UnitID"]),
                                          Description = dr["Description"].ToString(),
                                          Quantity = Convert.ToInt16(dr["Quantity"]),
                                          CreatedBy = dr["CreatedBy"].ToString(),
                                          ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"])
                                      }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public Menu getMenuDetails(string productId)
        {
            var model = new Menu();
            try
            {
                var cmdText = "MenuCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "StatementType", Value = SqlOperationEnum.SelectOne.ToString() });
                parameterList.Add(new SqlParameter { ParameterName = "ID", Value = productId });
                DataTable dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);

                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(productId);
                    model.CategoryID = Convert.ToInt64(dt.Rows[0]["CategoryID"]);
                    model.MerchantID = Convert.ToInt64(dt.Rows[0]["MerchantID"]);
                    model.ItemCode = dt.Rows[0]["ItemCode"].ToString();
                    model.ItemName = dt.Rows[0]["ItemName"].ToString();
                    model.Price = Convert.ToUInt16(dt.Rows[0]["Price"]);
                    model.UnitID = Convert.ToInt64(dt.Rows[0]["UnitID"]);
                    model.Description = dt.Rows[0]["Description"].ToString();
                    model.Quantity = Convert.ToInt32(dt.Rows[0]["Quantity"]);
                    model.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                    model.ModifiedDate = Convert.ToDateTime(dt.Rows[0]["ModifiedDate"]);
                    model.FoodType = dt.Rows[0]["FoodType"].ToString();
                    model.ItemImage = dt.Rows[0]["ItemImage"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public Menu getPreMenu(string unitId, string MerchantId, string categoryId)
        {
            var model = new Menu();
            try
            {
                var parameterList = new List<SqlParameter>();
                var cmdText = "select * from ShopTypeCategoryMaster as stc inner join MerchantCategoryAssign as mca on stc.ID = mca.ChildCategoryID " +
                              "where stc.Level1ParentCategoryId IS NOT NULL and mca.MerchantID = '" + MerchantId + "'";
                var dtCategory = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dtCategory != null && dtCategory.Rows.Count > 0)
                {
                    model.CategoryList = (from DataRow dr in dtCategory.Rows
                                          select new SelectListItem()
                                          {
                                              Value = dr["ID"].ToString(),
                                              Text = dr["CategoryName"].ToString(),
                                              Selected = !string.IsNullOrEmpty(categoryId) ? dr["ID"].ToString() == categoryId : false
                                          }).ToList();
                    if (model.CategoryList.Count > 0)
                    {
                        model.CategoryList.Insert(0, new SelectListItem()
                        {
                            Value = "",
                            Text = "Select Category",
                        });
                    }
                    else
                    {
                        return new Menu();
                    }
                }

                var cmdTextUnit = "Select * From UnitMaster as um inner join usermaster us ON um.MerchantID = us.ID where um.MerchantID = " + MerchantId;
                var dtUnit = DbProvider.GetDataTable(connectionString, cmdTextUnit, CommandType.Text, ref parameterList);
                if (dtUnit != null && dtUnit.Rows.Count > 0)
                {
                    model.UnitList = (from DataRow dr in dtUnit.Rows
                                      select new SelectListItem()
                                      {
                                          Value = dr["ID"].ToString(),
                                          Text = dr["UnitName"].ToString(),
                                          Selected = !string.IsNullOrEmpty(unitId) ? dr["ID"].ToString() == unitId : false
                                      }).ToList();
                    model.UnitList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Unit",
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Category Management
        public ShopTypeCategoryMasterView CategoryList(string merchantId, bool isParentCategory, string level1CategoryId)
        {
            var model = new ShopTypeCategoryMasterView();
            try
            {
                var cmdText = "MerchantCategoryAssignCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@Level1CategoryId", Value = level1CategoryId });
                parameterList.Add(new SqlParameter { ParameterName = "@IsParentCategory", Value = isParentCategory });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ShopTypeCatList = (from DataRow dr in dt.Rows
                                             select new ShopTypeCategoryMaster()
                                             {
                                                 ID = Convert.ToInt64(dr["CategoryID"]),
                                                 CategoryName = dr["CategoryName"].ToString(),
                                                 IsAssigned = !string.IsNullOrEmpty(dr["MerchantID"].ToString()) ? dr["MerchantID"].ToString() == merchantId : false
                                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UnAssignCategory(string categoryID, string merchantID)
        {
            var result = false;
            try
            {
                var cmdText = "MerchantCategoryAssignCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantID });
                parameterList.Add(new SqlParameter { ParameterName = "@ChildCategoryID", Value = categoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Delete.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public ShopTypeCategoryMaster GetPreAddCategoryAssign(bool isParentCategory, string merchantID, string categoryId)
        {
            var model = new ShopTypeCategoryMaster();
            try
            {
                var cmdText = "";
                if (isParentCategory)
                    cmdText = "select * from ShopTypeCategoryMaster where Level1ParentCategoryID IS NULL and ParentCategoryID IN (select stc.ID as userId from ShopTypeCategoryMaster as stc inner join UserMaster as um on stc.ID = um.ShopCategoryID where um.ID = " + merchantID + ")";
                else if (!string.IsNullOrEmpty(categoryId))
                    cmdText = "select * from ShopTypeCategoryMaster where Level1ParentCategoryID = '" + categoryId + "'";

                var parameterList = new List<SqlParameter>();

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.CategoryList = (from DataRow dr in dt.Rows
                                          select new SelectListItem()
                                          {
                                              Value = dr["ID"].ToString(),
                                              Text = dr["CategoryName"].ToString()
                                          }).ToList();
                    model.CategoryList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Category"
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool AssignCategory(string categoryID, string merchantID)
        {
            var result = false;
            try
            {
                var cmdText = "MerchantCategoryAssignCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantID });
                parameterList.Add(new SqlParameter { ParameterName = "@ChildCategoryID", Value = categoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });
                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public ShopTypeCategoryMaster GetPreAddChildCategory(string merchantId)
        {
            var model = new ShopTypeCategoryMaster();
            try
            {
                var cmdText = "MerchantCategoryAssignCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "AddChildCategory" });
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.Text, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.CategoryList = (from DataRow dr in dt.Rows
                                          select new SelectListItem()
                                          {
                                              Value = dr["ID"].ToString(),
                                              Text = dr["CategoryName"].ToString()
                                          }).ToList();
                    model.CategoryList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Category"
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateChildCategory(ShopTypeCategoryMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryName", Value = input.CategoryName });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryLogo", Value = input.CategoryLogo });
                parameterList.Add(new SqlParameter { ParameterName = "@Level1ParentCategoryId", Value = input.Level1ParentCategoryId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "InsertChildCategory" });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public MerchantMaster CheckCategoryNameExist(string catName)
        {
            var model = new MerchantMaster();
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryName", Value = catName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "CheckCategoryNameExist" });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion

        #region Order Management
        public OrderMasterView OrderList(string merchantId)
        {
            var model = new OrderMasterView();
            try
            {
                var cmdText = "OrderMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantId", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.OrderList = (from DataRow dr in dt.Rows
                                       select new OrderMaster()
                                       {
                                           ID = Convert.ToInt64(dr["OrderID"]),
                                           OrderNo = dr["OrderNo"].ToString(),
                                           OrderDate = !string.IsNullOrEmpty(dr["OrderDate"].ToString()) ? Convert.ToDateTime(dr["OrderDate"]) : DateTime.MinValue,
                                           CustomerName = dr["FirstName"].ToString() + " " + dr["LastName"].ToString()
                                       }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public OrderMaster GetOrderDetail(string orderId, string merchantId)
        {
            var model = new OrderMaster();
            try
            {
                var cmdText = "OrderMasterCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@OrderID", Value = orderId });
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantId", Value = merchantId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = "OrderDetailById" });
                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.OrderNo = dt.Rows[0]["OrderNo"].ToString();
                    model.OrderStatus = !string.IsNullOrEmpty(dt.Rows[0]["OrderStatus"].ToString()) ? Convert.ToInt32(dt.Rows[0]["OrderStatus"].ToString()) : 0;
                    model.OrderType = !string.IsNullOrEmpty(dt.Rows[0]["OrderType"].ToString()) ? Convert.ToInt32(dt.Rows[0]["OrderType"].ToString()) : 0;
                    model.PaymentMethod = !string.IsNullOrEmpty(dt.Rows[0]["PaymentMethod"].ToString()) ? Convert.ToInt32(dt.Rows[0]["PaymentMethod"].ToString()) : 0;
                    model.OrderDate = Convert.ToDateTime(dt.Rows[0]["OrderDate"].ToString());
                    model.Comment = dt.Rows[0]["Comment"].ToString();
                    model.CustomerName = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                    model.IsPaymentDone = !string.IsNullOrEmpty(dt.Rows[0]["IsPaymentDone"].ToString()) ? Convert.ToBoolean(dt.Rows[0]["IsPaymentDone"].ToString()) : false;
                    model.IsDelivery = !string.IsNullOrEmpty(dt.Rows[0]["IsDelivery"].ToString()) ? Convert.ToBoolean(dt.Rows[0]["IsDelivery"].ToString()) : false;
                    model.OrderItemList = (from DataRow dr in dt.Rows
                                           select new OrderItem()
                                           {
                                               ID = Convert.ToInt64(dr["ItemID"]),
                                               ItemName = dr["ItemName"].ToString(),
                                               Quantity = !string.IsNullOrEmpty(dr["Quantity"].ToString()) ? Convert.ToInt32(dr["Quantity"]) : 0,
                                               OrderQuantity = !string.IsNullOrEmpty(dr["OrderQuantity"].ToString()) ? Convert.ToInt32(dr["OrderQuantity"]) : 0,
                                               Price = float.Parse(dr["Price"].ToString()),
                                               Unit = dr["Unit"].ToString()
                                           }).ToList();
                    //Billing Details
                    model.BillingName = dt.Rows[0]["BillingFullName"].ToString();
                    model.BillingContactNo = dt.Rows[0]["BillingContactNo"].ToString();
                    model.BillingAddress = dt.Rows[0]["BillingAddress"].ToString();
                    model.BillingCity = dt.Rows[0]["BillingCity"].ToString();
                    model.BillingState = dt.Rows[0]["BillingState"].ToString();
                    model.BillingCountry = dt.Rows[0]["BillingCountry"].ToString();
                    model.BillingZipCode = dt.Rows[0]["BillingZipCode"].ToString();
                    //Delivery Details
                    model.DeliveryName = dt.Rows[0]["DeliveryFullName"].ToString();
                    model.DeliveryContactNo = dt.Rows[0]["DeliveryContactNo"].ToString();
                    model.DeliveryAddress = dt.Rows[0]["DeliveryAddress"].ToString();
                    model.DeliveryCity = dt.Rows[0]["DeliveryCity"].ToString();
                    model.DeliveryState = dt.Rows[0]["DeliveryState"].ToString();
                    model.DeliveryCountry = dt.Rows[0]["DeliveryCountry"].ToString();
                    model.DeliveryZipCode = dt.Rows[0]["DeliveryZipCode"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion
    }
}
