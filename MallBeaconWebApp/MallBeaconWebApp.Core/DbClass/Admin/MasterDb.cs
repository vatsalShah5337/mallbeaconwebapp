﻿using CarRentals.Core.DbConfigures;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbClass.Admin
{
    public class MasterDb
    {
        public string connectionString = string.Empty;
        public MasterDb(string conStr)
        {
            connectionString = conStr;
        }

        #region shop type category
        public ShopTypeCategoryMasterView ShopTypeCategoryList()
        {
            var model = new ShopTypeCategoryMasterView();
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ShopTypeCatList = (from DataRow dr in dt.Rows
                                             select new ShopTypeCategoryMaster()
                                             {
                                                 ID = Convert.ToInt64(dr["ID"]),
                                                 CategoryName = dr["CategoryName"].ToString(),
                                                 IsHomeVisible = !string.IsNullOrEmpty(dr["IsHomeVisible"].ToString()) ? Convert.ToBoolean(dr["IsHomeVisible"].ToString()) : false,
                                                 Level1ParentCategoryId = !string.IsNullOrEmpty(dr["Level1ParentCategoryId"].ToString()) ? Convert.ToInt64(dr["Level1ParentCategoryId"].ToString()) : 0
                                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateShopTypeCategory(ShopTypeCategoryMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryName", Value = input.CategoryName });
                parameterList.Add(new SqlParameter { ParameterName = "@ParentCategoryID", Value = input.ParentCategoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryLogo", Value = input.CategoryLogo });
                parameterList.Add(new SqlParameter { ParameterName = "@IsHomeVisible", Value = input.IsHomeVisible });
                parameterList.Add(new SqlParameter { ParameterName = "@DisplayType", Value = input.DisplayType });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public ShopTypeCategoryMaster GetShopTypeCategoryDetails(string shopTypeId)
        {
            var model = new ShopTypeCategoryMaster();
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = shopTypeId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.CategoryName = dt.Rows[0]["CategoryName"].ToString();
                    model.ParentCategoryID = string.IsNullOrEmpty(dt.Rows[0]["ParentCategoryID"].ToString()) ? 0 : Convert.ToInt64(dt.Rows[0]["ParentCategoryID"].ToString());
                    model.CategoryLogo = dt.Rows[0]["CategoryLogo"].ToString();
                    model.IsHomeVisible = !string.IsNullOrEmpty(dt.Rows[0]["IsHomeVisible"].ToString()) ? Convert.ToBoolean(dt.Rows[0]["IsHomeVisible"].ToString()) : false;
                    var result = getPreCategory(dt.Rows[0]["ParentCategoryID"].ToString(), dt.Rows[0]["DisplayType"].ToString());
                    model.CategoryList = result.CategoryList;
                    model.DisplayTypeList = result.DisplayTypeList;
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateShopTypeCategory(ShopTypeCategoryMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "ShopTypeCategoryMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryName", Value = input.CategoryName });
                parameterList.Add(new SqlParameter { ParameterName = "@ParentCategoryID", Value = input.ParentCategoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryLogo", Value = input.CategoryLogo});
                parameterList.Add(new SqlParameter { ParameterName = "@IsHomeVisible", Value = input.IsHomeVisible });
                parameterList.Add(new SqlParameter { ParameterName = "@DisplayType", Value = input.DisplayType });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public ShopTypeCategoryMaster getPreCategory(string catId, string displayType)
        {
            var model = new ShopTypeCategoryMaster();
            try
            {
                //Mall dropdown list
                var cmdText1 = "Select ID, CategoryName from ShopTypeCategoryMaster where ParentCategoryID IS NULL and Level1ParentCategoryID IS NULL";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.CategoryList = (from DataRow dr in dt1.Rows
                             select new SelectListItem()
                             {
                                 Value = dr["ID"].ToString(),
                                 Text = dr["CategoryName"].ToString(),
                                 Selected = !string.IsNullOrEmpty(catId) ? dr["ID"].ToString() == catId : false
                             }).ToList();
                    model.CategoryList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Parent Category",
                    });
                }
                model.DisplayTypeList.Add(new SelectListItem() { Value = "Product", Text = "Product", Selected = "Product" == displayType });
                model.DisplayTypeList.Add(new SelectListItem() { Value = "Menu", Text = "Menu", Selected = "Menu" == displayType });
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion


        #region amenities
        public AmenityMasterView AmenityList()
        {
            var model = new AmenityMasterView();
            try
            {
                var cmdText = "AmenityMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.AmenityList = (from DataRow dr in dt.Rows
                                             select new AmenityMaster()
                                             {
                                                 ID = Convert.ToInt64(dr["ID"]),
                                                 AmenityName = dr["AmenityName"].ToString()
                                             }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateAmenity(AmenityMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "AmenityMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@AmenityName", Value = input.AmenityName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public AmenityMaster GetAmenityDetails(string amenityId)
        {
            var model = new AmenityMaster();
            try
            {
                var cmdText = "AmenityMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = amenityId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.AmenityName = dt.Rows[0]["AmenityName"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateAmenity(AmenityMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "AmenityMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@AmenityName", Value = input.AmenityName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion


        #region Beacon
        public BeaconMasterView BeaconList()
        {
            var model = new BeaconMasterView();
            try
            {
                var cmdText = "BeaconMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.BeaconList = (from DataRow dr in dt.Rows
                                         select new BeaconMaster()
                                         {
                                             ID = Convert.ToInt64(dr["ID"]),
                                             MacAddress = dr["MacAddress"].ToString()
                                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateBeacon(BeaconMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "BeaconMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MacAddress", Value = input.MacAddress });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public BeaconMaster GetBeaconDetails(string beaconId)
        {
            var model = new BeaconMaster();
            try
            {
                var cmdText = "BeaconMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = beaconId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.MacAddress = dt.Rows[0]["MacAddress"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateBeacon(BeaconMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "BeaconMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@MacAddress", Value = input.MacAddress });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion


        #region Roles
        public RoleMasterView RoleList()
        {
            var model = new RoleMasterView();
            try
            {
                var cmdText = "RoleMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.RoleList = (from DataRow dr in dt.Rows
                                         select new RoleMaster()
                                         {
                                             ID = Convert.ToInt64(dr["ID"]),
                                             RoleName = dr["RoleName"].ToString()
                                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateRole(RoleMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "RoleMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@RoleName", Value = input.RoleName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public RoleMaster GetRoleDetails(string roleId)
        {
            var model = new RoleMaster();
            try
            {
                var cmdText = "RoleMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = roleId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.RoleName = dt.Rows[0]["RoleName"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateRole(RoleMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "RoleMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@RoleName", Value = input.RoleName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion


        #region Franchies
        public FranchiesMasterView FranchiesList()
        {
            var model = new FranchiesMasterView();
            try
            {
                var cmdText = "FranchiesMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.FranchiesList = (from DataRow dr in dt.Rows
                                      select new FranchiesMaster()
                                      {
                                          ID = Convert.ToInt64(dr["ID"]),
                                          FranchiesName = dr["FranchiesName"].ToString()
                                      }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool CreateFranchies(FranchiesMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "FranchiesMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@FranchiesName", Value = input.FranchiesName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public FranchiesMaster GetFranchiesDetails(string franchiesId)
        {
            var model = new FranchiesMaster();
            try
            {
                var cmdText = "FranchiesMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = franchiesId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.FranchiesName = dt.Rows[0]["FranchiesName"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateFranchies(FranchiesMaster input)
        {
            var result = false;
            try
            {
                var cmdText = "FranchiesMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@FranchiesName", Value = input.FranchiesName });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        #endregion


        #region Currency
        public bool CreateCurrency(CurrencyMaster input)
        {
            var status = true;
            try
            {
                var cmdText = "CurrencyMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@Name", Value = input.Name });
                parameterList.Add(new SqlParameter { ParameterName = "@ShortName", Value = input.ShortName });
                parameterList.Add(new SqlParameter { ParameterName = "@Symbol", Value = input.Symbol });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                    status = true;
                else
                    status = false;
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public bool UpdateCurrency(CurrencyMaster input)
        {
            var status = false;
            try
            {
                var cmdText = "CurrencyMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@Name", Value = input.Name });
                parameterList.Add(new SqlParameter { ParameterName = "@ShortName", Value = input.ShortName });
                parameterList.Add(new SqlParameter { ParameterName = "@Symbol", Value = input.Symbol });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                    status = true;
                else
                    status = false;
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public CurrencyMasterView CurrencyList()
        {
            var model = new CurrencyMasterView();
            try
            {
                var cmdText = "CurrencyMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.CurrencyMasterList = (from DataRow dr in dt.Rows
                                                select new CurrencyMaster()
                                                {
                                                    ID = Convert.ToInt64(dr["ID"]),
                                                    Name = dr["Name"].ToString(),
                                                    ShortName = dr["ShortName"].ToString(),
                                                    Symbol = dr["Symbol"].ToString()
                                                }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public CurrencyMaster GetCurrencyDetails(string currencyId)
        {
            var model = new CurrencyMaster();
            try
            {
                var cmdText = "CurrencyMasterCRUD";
                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = currencyId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"]);
                    model.Name = dt.Rows[0]["Name"].ToString();
                    model.ShortName = dt.Rows[0]["ShortName"].ToString();
                    model.Symbol = dt.Rows[0]["Symbol"].ToString();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }
        #endregion
    }
}
