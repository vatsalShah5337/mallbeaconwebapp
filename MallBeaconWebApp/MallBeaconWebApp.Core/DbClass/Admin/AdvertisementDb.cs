﻿using CarRentals.Core.DbConfigures;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbClass.Admin
{
    public class AdvertisementDb
    {
        public string connectionString = string.Empty;

        public AdvertisementDb(string _constr)
        {
            connectionString = _constr;
        }

        public bool CreateAdvertisement(Advertisement input)
        {
            var result = false;
            try
            {
                var cmdText = "AdvertisementCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@Title", Value = input.Title });
                parameterList.Add(new SqlParameter { ParameterName = "@Priority", Value = input.Priority });
                parameterList.Add(new SqlParameter { ParameterName = "@OuterImgPath", Value = input.OuterImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@InnerImgPath", Value = input.InnerImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterList.Add(new SqlParameter { ParameterName = "@EventStartDate", Value = input.EventStartDate });
                parameterList.Add(new SqlParameter { ParameterName = "@EventEndDate", Value = input.EventEndDate });
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public AdvertisementView ListAdvertisement()
        {
            var model = new AdvertisementView();
            try
            {
                var cmdText = "AdvertisementCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.AdvertisementList = (from DataRow dr in dt.Rows
                                               select new Advertisement()
                                               {
                                                   ID = Convert.ToInt64(dr["ID"]),
                                                   MallName = dr["MallName"].ToString(),
                                                   MallID = Convert.ToInt64(dr["MallID"].ToString()),
                                                   Description = dr["Description"].ToString(),
                                                   EventEndDate = Convert.ToDateTime(dr["EventEndDate"].ToString()),
                                                   EventStartDate = Convert.ToDateTime(dr["EventStartDate"].ToString()),
                                                   InnerImgPath = dr["InnerImgPath"].ToString(),
                                                   OuterImgPath = dr["OuterImgPath"].ToString(),
                                                   Title = dr["Title"].ToString()
                                               }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public Advertisement GetAdvertisementDetails(string advertisementId)
        {
            var model = new Advertisement();
            try
            {
                var cmdText = "AdvertisementCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = advertisementId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.MallID = Convert.ToInt64(dt.Rows[0]["MallID"].ToString());
                    model.Priority = !string.IsNullOrEmpty(dt.Rows[0]["Priority"].ToString()) ? Convert.ToInt32(dt.Rows[0]["Priority"].ToString()) : 0;
                    model.Description = dt.Rows[0]["Description"].ToString();
                    model.EventEndDate = Convert.ToDateTime(dt.Rows[0]["EventEndDate"].ToString());
                    model.EventStartDate = Convert.ToDateTime(dt.Rows[0]["EventStartDate"].ToString());
                    model.InnerImgPath = dt.Rows[0]["InnerImgPath"].ToString();
                    model.OuterImgPath = dt.Rows[0]["OuterImgPath"].ToString();
                    model.Title = dt.Rows[0]["Title"].ToString();
                    model.MerchantID = !string.IsNullOrEmpty(dt.Rows[0]["MerchantID"].ToString()) ? Convert.ToInt64(dt.Rows[0]["MerchantID"].ToString()) : 0;
                    var result = getPreAdvertisement(model.MallID.ToString());
                    model.MallList = result.MallList;
                    model.MerchantList = result.MerchantList;
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateAdvertisement(Advertisement input)
        {
            var result = false;
            try
            {
                var cmdText = "AdvertisementCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@Priority", Value = input.Priority });
                parameterList.Add(new SqlParameter { ParameterName = "@Title", Value = input.Title });
                parameterList.Add(new SqlParameter { ParameterName = "@OuterImgPath", Value = input.OuterImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@InnerImgPath", Value = input.InnerImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterList.Add(new SqlParameter { ParameterName = "@EventStartDate", Value = input.EventStartDate });
                parameterList.Add(new SqlParameter { ParameterName = "@EventEndDate", Value = input.EventEndDate });
                parameterList.Add(new SqlParameter { ParameterName = "@MerchantID", Value = input.MerchantID });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public Advertisement getPreAdvertisement(string mallID)
        {
            var model = new Advertisement();
            try
            {
                //Mall dropdown list
                var cmdText1 = "Select * from MallMaster";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.MallList = (from DataRow dr in dt1.Rows
                                      select new SelectListItem()
                                      {
                                          Value = dr["ID"].ToString(),
                                          Text = dr["MallName"].ToString(),
                                          Selected = !string.IsNullOrEmpty(mallID) ? dr["ID"].ToString() == mallID : false
                                      }).ToList();
                    model.MallList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Mall",
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool DeleteAdvertisement(string advertisementId)
        {
            var result = false;
            try
            {
                var cmdText = "AdvertisementCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = advertisementId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Delete.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public List<string> CheckAdPriority(DateTime startDate, DateTime endDate)
        {
            var result = new List<string>();
            try
            {
                //Mall dropdown list
                var cmdText1 = "select * from advertisement where eventstartdate <= convert(date, '" + startDate + "', 103) and EventEndDate >= convert(date, '" + endDate + "', 103)";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (var a = 0; a < dt1.Rows.Count; a++)
                    {
                        if (!string.IsNullOrEmpty(dt1.Rows[a]["Priority"].ToString()))
                        {
                            result.Add(dt1.Rows[a]["Priority"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = new List<string>();
            }
            return result;
        }

        public List<SelectListItem> GetMerchantsbyMallID(string mallid)
        {
            var result = new List<SelectListItem>();
            try
            {
                //Mall dropdown list
                var cmdText1 = "select * from UserMaster where RoleID = 2 and MallID = " + mallid;
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (var a = 0; a < dt1.Rows.Count; a++)
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = dt1.Rows[a]["ShopName"].ToString(),
                            Value = dt1.Rows[a]["ID"].ToString()
                        });
                    }
                }
                result.Insert(0, new SelectListItem()
                {
                    Value = "",
                    Text = "Select Merchant",
                });
            }
            catch (Exception ex)
            {
                result = new List<SelectListItem>();
            }
            return result;
        }
    }
}
