﻿using CarRentals.Core.DbConfigures;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallBeaconWebApp.Core.DbClass.Admin
{
    public class MallDb
    {
        public string connectionString = string.Empty;
        public MallDb(string conStr)
        {
            connectionString = conStr;
        }

        public bool CreateMall(MallDeatils input)
        {
            var result = false;
            try
            {
                var cmdText = "MallMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MallName", Value = input.MallName });
                parameterList.Add(new SqlParameter { ParameterName = "@LocationLat", Value = input.LocationLat });
                parameterList.Add(new SqlParameter { ParameterName = "@LocationLong", Value = input.LocationLong });
                parameterList.Add(new SqlParameter { ParameterName = "@Address", Value = input.Address });
                parameterList.Add(new SqlParameter { ParameterName = "@City", Value = input.City });
                parameterList.Add(new SqlParameter { ParameterName = "@State", Value = input.State });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerName", Value = input.OwnerName });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerEmail", Value = input.OwnerEmail });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerContactNo", Value = input.OwnerContactNo });
                parameterList.Add(new SqlParameter { ParameterName = "@MallArea", Value = input.MallArea });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopLeaseDuration", Value = input.ShopLeaseDuration });
                parameterList.Add(new SqlParameter { ParameterName = "@TotalNoShops", Value = input.TotalNoShops });
                parameterList.Add(new SqlParameter { ParameterName = "@BuilderGroupName", Value = input.BuilderGroupName });
                parameterList.Add(new SqlParameter { ParameterName = "@MallWebsite", Value = input.MallWebsite });
                parameterList.Add(new SqlParameter { ParameterName = "@ParkingSpace", Value = input.ParkingSpace });
                parameterList.Add(new SqlParameter { ParameterName = "@MallLogo", Value = input.MallLogo });
                parameterList.Add(new SqlParameter { ParameterName = "@MallImage", Value = input.MallImage });
                parameterList.Add(new SqlParameter { ParameterName = "@IsActive", Value = input.IsActive });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public MallDetailsView ListMall()
        {
            var model = new MallDetailsView();
            try
            {
                var cmdText = "MallMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.MallDetailsList = (from DataRow dr in dt.Rows
                                         select new MallDeatils()
                                         {
                                             ID = Convert.ToInt64(dr["ID"]),
                                             MallName = dr["MallName"].ToString(),
                                             LocationLat = float.Parse(dr["LocationLat"].ToString()),
                                             LocationLong = float.Parse(dr["LocationLong"].ToString()),
                                             Address = dr["Address"].ToString(),
                                             City = dr["City"].ToString(),
                                             State = dr["State"].ToString(),
                                             OwnerName = dr["OwnerName"].ToString(),
                                             OwnerEmail = dr["OwnerEmail"].ToString(),
                                             OwnerContactNo = dr["OwnerContactNo"].ToString(),
                                             MallArea = dr["MallArea"].ToString(),
                                             ShopLeaseDuration = dr["ShopLeaseDuration"].ToString(),
                                             TotalNoShops = Convert.ToInt32(dr["TotalNoShops"].ToString()),
                                             BuilderGroupName = dr["BuilderGroupName"].ToString(),
                                             MallWebsite = dr["MallWebsite"].ToString(),
                                             ParkingSpace = dr["ParkingSpace"].ToString()
                                         }).ToList();
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public MallDeatils GetMallDetails(string mallId)
        {
            var model = new MallDeatils();
            try
            {
                var cmdText = "MallMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = mallId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.MallName = dt.Rows[0]["MallName"].ToString();
                    model.LocationLat = float.Parse(dt.Rows[0]["LocationLat"].ToString());
                    model.LocationLong = float.Parse(dt.Rows[0]["LocationLong"].ToString());
                    model.Address = dt.Rows[0]["Address"].ToString();
                    model.City = dt.Rows[0]["City"].ToString();
                    model.State = dt.Rows[0]["State"].ToString();
                    model.OwnerName = dt.Rows[0]["OwnerName"].ToString();
                    model.OwnerEmail = dt.Rows[0]["OwnerEmail"].ToString();
                    model.OwnerContactNo = dt.Rows[0]["OwnerContactNo"].ToString();
                    model.MallArea = dt.Rows[0]["MallArea"].ToString();
                    model.ShopLeaseDuration = dt.Rows[0]["ShopLeaseDuration"].ToString();
                    model.TotalNoShops = Convert.ToInt32(dt.Rows[0]["TotalNoShops"].ToString());
                    model.BuilderGroupName = dt.Rows[0]["BuilderGroupName"].ToString();
                    model.MallWebsite = dt.Rows[0]["MallWebsite"].ToString();
                    model.ParkingSpace = dt.Rows[0]["ParkingSpace"].ToString();
                    model.MallLogo = dt.Rows[0]["MallLogo"].ToString();
                    model.MallImage = dt.Rows[0]["MallImage"].ToString();
                    model.IsActive = !string.IsNullOrEmpty(dt.Rows[0]["IsActive"].ToString()) ? Convert.ToBoolean(dt.Rows[0]["IsActive"].ToString()) : false;
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateMall(MallDeatils input)
        {
            var result = false;
            try
            {
                var cmdText = "MallMasterCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@MallName", Value = input.MallName });
                parameterList.Add(new SqlParameter { ParameterName = "@LocationLat", Value = input.LocationLat });
                parameterList.Add(new SqlParameter { ParameterName = "@LocationLong", Value = input.LocationLong });
                parameterList.Add(new SqlParameter { ParameterName = "@Address", Value = input.Address });
                parameterList.Add(new SqlParameter { ParameterName = "@City", Value = input.City });
                parameterList.Add(new SqlParameter { ParameterName = "@State", Value = input.State });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerName", Value = input.OwnerName });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerEmail", Value = input.OwnerEmail });
                parameterList.Add(new SqlParameter { ParameterName = "@OwnerContactNo", Value = input.OwnerContactNo });
                parameterList.Add(new SqlParameter { ParameterName = "@MallArea", Value = input.MallArea });
                parameterList.Add(new SqlParameter { ParameterName = "@ShopLeaseDuration", Value = input.ShopLeaseDuration });
                parameterList.Add(new SqlParameter { ParameterName = "@TotalNoShops", Value = input.TotalNoShops });
                parameterList.Add(new SqlParameter { ParameterName = "@BuilderGroupName", Value = input.BuilderGroupName });
                parameterList.Add(new SqlParameter { ParameterName = "@MallWebsite", Value = input.MallWebsite });
                parameterList.Add(new SqlParameter { ParameterName = "@ParkingSpace", Value = input.ParkingSpace });
                parameterList.Add(new SqlParameter { ParameterName = "@MallLogo", Value = input.MallLogo });
                parameterList.Add(new SqlParameter { ParameterName = "@MallImage", Value = input.MallImage });
                parameterList.Add(new SqlParameter { ParameterName = "@IsActive", Value = input.IsActive });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}
