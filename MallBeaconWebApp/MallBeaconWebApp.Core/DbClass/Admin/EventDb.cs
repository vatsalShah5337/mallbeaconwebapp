﻿using CarRentals.Core.DbConfigures;
using MallBeaconWebApp.Core.DbProperties.Admin;
using MallBeaconWebApp.Core.DbTblViews.Admin;
using MallBeaconWebApp.Core.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MallBeaconWebApp.Core.DbClass.Admin
{
    public class EventDb
    {
        public string connectionString = string.Empty;

        public EventDb(string _constr)
        {
            connectionString = _constr;
        }

        public bool CreateEvent(Event input)
        {
            var result = false;
            try
            {
                var cmdText = "TrendEventCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@Title", Value = input.Title });
                parameterList.Add(new SqlParameter { ParameterName = "@OuterImgPath", Value = input.OuterImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@InnerImgPath", Value = input.InnerImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterList.Add(new SqlParameter { ParameterName = "@EventStartDate", Value = input.EventStartDate });
                parameterList.Add(new SqlParameter { ParameterName = "@EventEndDate", Value = input.EventEndDate });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryID", Value = input.CategoryID });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Insert.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public EventView ListEvent(string type)
        {
            var model = new EventView();
            try
            {
                var cmdText = "TrendEventCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@Type", Value = type });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectAll.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.EventList = (from DataRow dr in dt.Rows
                                       select new Event()
                                       {
                                           ID = Convert.ToInt64(dr["ID"]),
                                           MallName = dr["MallName"].ToString(),
                                           MallID = Convert.ToInt64(dr["MallID"].ToString()),
                                           Description = dr["Description"].ToString(),
                                           EventEndDate = Convert.ToDateTime(dr["EventEndDate"].ToString()),
                                           EventStartDate = Convert.ToDateTime(dr["EventStartDate"].ToString()),
                                           InnerImgPath = dr["InnerImgPath"].ToString(),
                                           OuterImgPath = dr["OuterImgPath"].ToString(),
                                           Title = dr["Title"].ToString(),
                                           CategoryID = type == "Category" ? Convert.ToInt64(dr["CategoryID"].ToString()) : 0,
                                           CategoryName = type == "Category" ? dr["CategoryName"].ToString() : ""
                                       }).ToList();
                }
                model.EventType = type;
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public Event GetEventDetails(string eventId)
        {
            var model = new Event();
            try
            {
                var cmdText = "TrendEventCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = eventId });
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.SelectOne.ToString() });

                var dt = DbProvider.GetDataTable(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ID = Convert.ToInt64(dt.Rows[0]["ID"].ToString());
                    model.MallID = Convert.ToInt64(dt.Rows[0]["MallID"].ToString());
                    model.Description = dt.Rows[0]["Description"].ToString();
                    model.EventEndDate = Convert.ToDateTime(dt.Rows[0]["EventEndDate"].ToString());
                    model.EventStartDate = Convert.ToDateTime(dt.Rows[0]["EventStartDate"].ToString());
                    model.InnerImgPath = dt.Rows[0]["InnerImgPath"].ToString();
                    model.OuterImgPath = dt.Rows[0]["OuterImgPath"].ToString();
                    model.Title = dt.Rows[0]["Title"].ToString();
                    model.CategoryID = !string.IsNullOrEmpty(dt.Rows[0]["CategoryID"].ToString()) ? Convert.ToInt64(dt.Rows[0]["CategoryID"].ToString()) : 0;
                    var result = getPreEvent(model.MallID.ToString(), model.CategoryID.ToString());
                    model.CategoryList = result.CategoryList;
                    model.MallList = result.MallList;
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool UpdateEvent(Event input)
        {
            var result = false;
            try
            {
                var cmdText = "TrendEventCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = input.ID });
                parameterList.Add(new SqlParameter { ParameterName = "@MallID", Value = input.MallID });
                parameterList.Add(new SqlParameter { ParameterName = "@Title", Value = input.Title });
                parameterList.Add(new SqlParameter { ParameterName = "@OuterImgPath", Value = input.OuterImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@InnerImgPath", Value = input.InnerImgPath });
                parameterList.Add(new SqlParameter { ParameterName = "@Description", Value = input.Description });
                parameterList.Add(new SqlParameter { ParameterName = "@EventStartDate", Value = input.EventStartDate });
                parameterList.Add(new SqlParameter { ParameterName = "@EventEndDate", Value = input.EventEndDate });
                parameterList.Add(new SqlParameter { ParameterName = "@CategoryID", Value = input.CategoryID });

                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Update.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        public Event getPreEvent(string mallID, string catID)
        {
            var model = new Event();
            try
            {
                //Mall dropdown list
                var cmdText1 = "Select * from MallMaster";
                var parameterList = new List<SqlParameter>();
                var dt1 = DbProvider.GetDataTable(connectionString, cmdText1, CommandType.Text, ref parameterList);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    model.MallList = (from DataRow dr in dt1.Rows
                                          select new SelectListItem()
                                          {
                                              Value = dr["ID"].ToString(),
                                              Text = dr["MallName"].ToString(),
                                              Selected = !string.IsNullOrEmpty(mallID) ? dr["ID"].ToString() == mallID: false
                                          }).ToList();
                    model.MallList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Mall",
                    });
                }

                //Mall dropdown list
                var cmdText2 = "Select ID, CategoryName from ShopTypeCategoryMaster";
                var parameterList1 = new List<SqlParameter>();
                var dt2 = DbProvider.GetDataTable(connectionString, cmdText2, CommandType.Text, ref parameterList1);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    model.CategoryList = (from DataRow dr in dt2.Rows
                             select new SelectListItem()
                             {
                                 Value = dr["ID"].ToString(),
                                 Text = dr["CategoryName"].ToString(),
                                 Selected = !string.IsNullOrEmpty(catID) ? dr["ID"].ToString() == catID : false
                             }).ToList();
                    model.CategoryList.Insert(0, new SelectListItem()
                    {
                        Value = "",
                        Text = "Select Category",
                    });
                }
            }
            catch (Exception ex)
            {
                model = null;
            }
            return model;
        }

        public bool DeleteEvent(string eventId)
        {
            var result = false;
            try
            {
                var cmdText = "TrendEventCRUD";

                var parameterList = new List<SqlParameter>();
                parameterList.Add(new SqlParameter { ParameterName = "@ID", Value = eventId});
                parameterList.Add(new SqlParameter { ParameterName = "@StatementType", Value = SqlOperationEnum.Delete.ToString() });

                var dbOutput = DbProvider.ExecuteNonQuery(connectionString, cmdText, CommandType.StoredProcedure, ref parameterList);
                if (dbOutput > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
    }
}
